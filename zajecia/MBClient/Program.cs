﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MBClient
{
    class Program
    {
        static void Main(string[] args)
        {
            //Flaga(args);
            FlagaMB(args);
        }

        static void Flaga(string[] args)
        {
            float[,] p = JasnośćObrazu("POLA0001.GIF", 10, 10);
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                    Console.Write(p[j, i] < 0.6f ? "##" : "..");
                Console.WriteLine();
            }
        }
        public static float[,] JasnośćObrazu(string plik, int szer, int wys)
        {
            float[,] punkty = new float[szer, wys];
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(plik);
            int sz = bmp.Width / szer;
            int wy = bmp.Height / wys;
            for (int i = 0; i < wys; i++)
                for (int j = 0; j < szer; j++)
                {
                    punkty[i, j] = 0;
                    for (int k = 0; k < wy; k++)
                        for (int l = 0; l < sz; l++)
                            punkty[i, j] += bmp.GetPixel(j * sz + l, i * wy + k).GetBrightness();
                    punkty[i, j] /= wy * sz;
                }
            return punkty;
        }

        static void FlagaMB(string[] args)
        {
            ModbusClient client = new ModbusTCPClient("localhost", 502);

            float[,] p = JasnośćObrazu("POLA0001.GIF", 10, 10);
            for (int i = 0; i < 10; i++)
            {
                ushort[] data = Enumerable.Range(0, 10).Select(x => (ushort)(p[i, x] < 0.6f ? 88 : 1)).ToArray();
                ModbusMessage msg = new WMRRequest(data, i*10);
                ModbusMessage ans = client.QA(msg);
                Console.WriteLine(ans.Function == msg.Function ? "ok" : "error");
            }

            client.Close();
        }

    }
}
