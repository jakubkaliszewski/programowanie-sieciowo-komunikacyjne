﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.IO;
using System.Threading;

namespace PSK
{
    public class Receive
    {
        public static string ReceiveUntilStop(UdpClient c)
        {
            String strData = "";
            String Ret = "";
            ASCIIEncoding ASCII = new ASCIIEncoding();

            IPEndPoint endpoint = new IPEndPoint(IPAddress.IPv6Any, 50);

            while (!strData.Equals("Over"))
            {
                Byte[] data = c.Receive(ref endpoint);
                strData = ASCII.GetString(data);
                Ret += strData + "\n";
            }
            return Ret;
        }
    }

    public class Send
    {
        private static char[] greetings = { 'H', 'e', 'l', 'l', 'o', ' ',
                                      'T', 'a', 'r', 'g', 'e', 't', '.' };
        private static char[] nice = { 'H', 'a', 'v', 'e', ' ', 'a', ' ', 'n', 'i',
                                      'c', 'e', ' ', 'd', 'a', 'y', '.' };
        private static char[] eom = { 'O', 'v', 'e', 'r' };

        private static char[] tGreetings = { 'H', 'e', 'l', 'l', 'o', ' ',
                                       'O', 'r', 'i', 'g', 'i', 'n', 'a', 't', 'o', 'r', '!' };
        private static char[] tNice = { 'Y', 'o', 'u', ' ', 't', 'o', 'o', '.' };

        public static void OriginatorSendData(UdpClient c, IPEndPoint ep)
        {
            Console.WriteLine(new string(greetings));
            c.Send(GetByteArray(greetings), greetings.Length, ep);
            Thread.Sleep(1000);

            Console.WriteLine(new String(nice));
            c.Send(GetByteArray(nice), nice.Length, ep);

            Thread.Sleep(1000);
            Console.WriteLine(new String(eom));
            c.Send(GetByteArray(eom), eom.Length, ep);
        }

        public static void TargetSendData(UdpClient c, IPEndPoint ep)
        {
            Console.WriteLine(new string(tGreetings));
            c.Send(GetByteArray(tGreetings), tGreetings.Length, ep);
            Thread.Sleep(1000);

            Console.WriteLine(new String(tNice));
            c.Send(GetByteArray(tNice), tNice.Length, ep);

            Thread.Sleep(1000);
            Console.WriteLine(new String(eom));
            c.Send(GetByteArray(eom), eom.Length, ep);
        }
        private static Byte[] GetByteArray(Char[] ChArray)
        {
            Byte[] Ret = new Byte[ChArray.Length];
            for (int i = 0; i < ChArray.Length; i++)
                Ret[i] = (Byte)ChArray[i];
            return Ret;
        }
    }


    public class ClientTarget
    {
        private static UdpClient m_ClientTarget;
        private static IPAddress m_GrpAddr;

        public static void MainTarget()
        {
            string Ret;

            m_ClientTarget = new UdpClient(1000, AddressFamily.InterNetworkV6);

            m_GrpAddr = IPAddress.Parse("FF01::1");

            m_ClientTarget.JoinMulticastGroup(m_GrpAddr);

            IPEndPoint ClientOriginatordest = new IPEndPoint(m_GrpAddr, 2000);

            Ret = Receive.ReceiveUntilStop(m_ClientTarget);
            Console.WriteLine("\nThe ClientTarget received: " + "\n\n" + Ret + "\n");


            Thread.Sleep(2000);

            Console.WriteLine("\nThe ClientTarget sent:\n");

            Send.TargetSendData(m_ClientTarget, ClientOriginatordest);

            m_ClientTarget.DropMulticastGroup(m_GrpAddr);
        }
    }


    public class ClientOriginator
    {
        private static UdpClient clientOriginator;
        private static IPAddress m_GrpAddr;
        private static IPEndPoint m_ClientTargetdest;

        public static bool MainOriginator()
        {
            try
            {
                clientOriginator = new UdpClient(2000, AddressFamily.InterNetworkV6);

                m_GrpAddr = IPAddress.Parse("FF01::1");

                Console.WriteLine("Multicast Address: [" + m_GrpAddr.ToString() + "]");

                Console.WriteLine("Instantiate IPv6MulticastOption(IPAddress)");

                IPv6MulticastOption ipv6MulticastOption = new IPv6MulticastOption(m_GrpAddr);

                IPAddress group = ipv6MulticastOption.Group;
                long interfaceIndex = ipv6MulticastOption.InterfaceIndex;

                Console.WriteLine("IPv6MulticastOption.Group: [" + group + "]");
                Console.WriteLine("IPv6MulticastOption.InterfaceIndex: [" + interfaceIndex + "]");

                IPv6MulticastOption ipv6MulticastOption2 = new IPv6MulticastOption(group, interfaceIndex);

                group = ipv6MulticastOption2.Group;
                interfaceIndex = ipv6MulticastOption2.InterfaceIndex;

                Console.WriteLine("IPv6MulticastOption.Group: [" + group + "]");
                Console.WriteLine("IPv6MulticastOption.InterfaceIndex: [" + interfaceIndex + "]");

                clientOriginator.JoinMulticastGroup((int)interfaceIndex, group);

                m_ClientTargetdest = new IPEndPoint(m_GrpAddr, 1000);

                Thread.Sleep(2000);

                SendAndReceive();

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("[ClientOriginator.ConnectClients] Exception: " + e.ToString());
                return false;
            }
        }

        public static string SendAndReceive()
        {
            string Ret = "";


            Console.WriteLine("\nThe ClientOriginator sent:\n");
            Send.OriginatorSendData(clientOriginator, m_ClientTargetdest);

            Ret = Receive.ReceiveUntilStop(clientOriginator);

            clientOriginator.DropMulticastGroup(m_GrpAddr);

            Console.WriteLine("\nThe ClientOriginator received: " + "\n\n" + Ret);

            return Ret;
        }
    }
}
