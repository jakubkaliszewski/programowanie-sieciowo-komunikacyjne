﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    public delegate string CommandD(string command);
    public delegate void CommunicatorD(ICommunicator commander);

    public interface ICommandAnswerer
    {
        string AnswerCommand(string command);
    }

    public interface ICommunicator
    {
        void Start(CommandD onCommand, CommunicatorD onDisconnect);
        void Stop();
    }

    public class ConsoleCommunicator : ICommunicator
    {
        public void Start(CommandD onCommand, CommunicatorD onDisconnect)
        {
            // czytamy polecenia i wykonujemy
            while (true)
            {
                string command = Console.ReadLine();
                Console.WriteLine(onCommand(command));
            }
        }

        public void Stop()
        {
        }
    }

}
