﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    class Klient
    {
        static ICommandAnswerer communicator;
        static void Main(string[] args)
        {
            //MainTCP(args);
            //UDP.UDPTestClient();
            ClientOriginator.MainOriginator();
        }
        static void MainTCP(string[] args)
        {
            communicator = new TCPCom("localhost", 12345);
            ConsoleCommunicator cc = new ConsoleCommunicator();
            cc.Start(UsługaPing, null);
        }

        private static string UsługaPing(string command)
        {            
            command += "\n";            
            return communicator.AnswerCommand(command);
        }
    }
}
