﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    public class TCPCommunicator : ICommunicator
    {
        private TcpClient client;
        public TCPCommunicator(TcpClient client)
        {
            this.client = client;
        }
        public void Start(CommandD onCommand, CommunicatorD onDisconnect)
        {
            byte[] bytes = new byte[256];
            string data = null;
            int len, nl;
            NetworkStream stream = client.GetStream();
            while ((len = stream.Read(bytes, 0, bytes.Length)) > 0)
            {
                data += Encoding.ASCII.GetString(bytes, 0, len);
                while ((nl = data.IndexOf('\n')) != -1)
                {
                    string line = data.Substring(0, nl + 1);
                    data = data.Substring(nl + 1);
                    byte[] msg = Encoding.ASCII.GetBytes(onCommand(line));
                    stream.Write(msg, 0, msg.Length);
                }
            }
            client.Close();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

    }
}
