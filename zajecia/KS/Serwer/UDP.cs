﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    class UDP
    {
        public static void UDPTestSrv()
        {
            UdpClient udpClient = new UdpClient(11000);
            Byte[] sendBytes = Encoding.ASCII.GetBytes("Welcome!");

            while (true)
                try
                {
                    IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                    Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                    string returnData = Encoding.ASCII.GetString(receiveBytes);

                    Console.WriteLine("This is the message you received " +
                                 returnData.ToString());
                    Console.WriteLine("This message was sent from " +
                                                RemoteIpEndPoint.Address.ToString() +
                                                " on their port number " +
                                                RemoteIpEndPoint.Port.ToString());

                    udpClient.Send(sendBytes, sendBytes.Length, RemoteIpEndPoint);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            udpClient.Close();
        }
    }
}
