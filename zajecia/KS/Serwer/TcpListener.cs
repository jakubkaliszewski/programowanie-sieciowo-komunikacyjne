﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    public class TCPListener : IListener
    {
        private TcpListener server;
        public void Start(CommunicatorD onConnect)
        {
            server = new TcpListener(IPAddress.Any, 12345);
            server.Start();

            
            while (true)
            {
                TcpClient client = server.AcceptTcpClient();
                TCPCommunicator communicator = new TCPCommunicator(client);
                onConnect(communicator);    
            }
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }


    }
}
