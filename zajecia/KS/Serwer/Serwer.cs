﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSK
{
    class Serwer
    {
        List<IListener> listeners = new List<IListener>();
        List<ICommunicator> communicators = new List<ICommunicator>();
        Dictionary<string, ICommandAnswerer> services = new Dictionary<string, ICommandAnswerer>();

        void AddListener(IListener listener)
        {
            listeners.Add(listener);
            listener.Start(AddCommunicator);
        }

        void AddCommunicator(ICommunicator commander)
        {
            communicators.Add(commander);
            commander.Start(ServiceCenter, CommunicatorDisconnect);
        }

        private void CommunicatorDisconnect(ICommunicator commander)
        {
            communicators.Remove(commander);
        }

        void AddService(string name, ICommandAnswerer service)
        {
            services.Add(name, service);
        }

        Serwer()
        {
            // usługa konfiguracji
            ConfigService cs = new ConfigService();
            AddService("cfg", cs);
        }

        public void Run()
        {
            TCPListener tcp_listener = new TCPListener();
            AddListener(tcp_listener);

            // odpowiadacz konsolowy
            ConsoleCommunicator cc = new ConsoleCommunicator();
            AddCommunicator(cc);
        }

        string ServiceCenter(string command)
        {
            // która usługa?
            string serviceId = command.Split()[0];
            ICommandAnswerer service;
            services.TryGetValue(serviceId, out service);
            if (service == null)
                return "ERROR: unknown service\n";
            // pytamy
            string ret = service.AnswerCommand(command);
            if (!ret.EndsWith("\n"))
            {
                ret += "\n";
            }
            return ret;
        }
        static void Main(string[] args)
        {
            //Serwer serwer = new Serwer();
            //serwer.Run();

            //UDP.UDPTestSrv();
            ClientTarget.MainTarget();
        }
    }
}
