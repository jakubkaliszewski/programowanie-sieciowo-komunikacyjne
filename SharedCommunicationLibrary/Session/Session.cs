using System;
using SharedCommunicationLibrary.Connections;

namespace SharedCommunicationLibrary.Session
{
    /// <summary>
    /// Reprezentacja sesji połączenia. Medium uniwersalne.
    /// </summary>
    public class Session
    {
        private DateTime _expirationTime;
        private Connection _connection;
        private Guid _sessionID;

        public Session(Connection connection)
        {
            _connection = connection;
            _expirationTime = DateTime.UtcNow;
            AddToExpirationTime();
            _sessionID = Guid.NewGuid();            
        }

        public Session(Connection connection, string sessionId)
        {
            _connection = connection;
            _expirationTime = DateTime.UtcNow;
            AddToExpirationTime();
            _sessionID = Guid.Parse(sessionId);    
        }

        public string ID => _sessionID.ToString();
        public Connection Connection => _connection;
        
        private void AddToExpirationTime()
        {
            _expirationTime.AddSeconds(120);
        }

        public void Close()
        {
            _connection.Medium.Disconnect();
            _connection.Medium.Dispose();
            _connection = null;
            _expirationTime = DateTime.UtcNow;
        }
        
        
        
        //jakieś zadarzenie jeśli czas wygaśnie by zamknąć sesję i wywołać także reakcję w menedżerze sesji
        
    }
}