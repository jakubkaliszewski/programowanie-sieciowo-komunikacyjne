using System;
using System.Text;
using SharedCommunicationLibrary.Services;
using SharedCommunicationLibrary.Session;

namespace SharedCommunicationLibrary.Message
{
    /// <summary>
    /// Klasa reprezentująca uniwersalną wiadomość dla każdej z usług.
    /// </summary>
    public class Message
    {
        protected string _id;
        protected string _sessionID;
        protected string _serviceName;
        protected string _content;
        protected string _checksum;
        protected bool _editable;
        protected readonly char _separator = ';';

        /// <summary>
        /// Konstruktor do tworzenia wiadomości odbieranej z medium.
        /// </summary>
        /// <param name="messageId">Identyfikator wiadomości odebranej</param>
        /// <param name="serviceName">Nazwa serwisu, który korzysta z wiadomości.</param>
        /// <param name="sessionId">Identyfikator sesji w ramach, której wiadomość została otrzymana.</param>
        /// <exception cref="Exception">Wyjatek rzucany w przypadku, gdy podany serwis nie istnieje bądź podany identyfikator sesji jest błędny.</exception>
        public Message(string messageId, string serviceName, string sessionId)
        {
            if (!serviceName.Equals("init"))
            {
                bool serviceNameisValid = ServiceNameIsValid(serviceName);
                if (!serviceNameisValid)
                    throw new Exception("Podany typ serwisu nie istnieje!! Niepowodzenie przy tworzeniu wiadomości!!");
                if (sessionId == null || !SessionIsValid(sessionId))
                    throw new Exception("ID sesji jest pusty!!");
            }

            _id = messageId;
            _serviceName = serviceName;
            _sessionID = sessionId;
            _editable = true;
        }

        /// <summary>
        /// Konstruktor do tworzenia wiadomości nadawanej.
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="sessionId"></param>
        /// <exception cref="Exception"></exception>
        public Message(string serviceName, string sessionId)
        {
            if (!serviceName.Equals("init"))
            {
                bool serviceNameisValid = ServiceNameIsValid(serviceName);
                if (!serviceNameisValid)
                    throw new Exception("Podany typ serwisu nie istnieje!! Niepowodzenie przy tworzeniu wiadomości!!");
                if (sessionId == null)
                    throw new Exception("ID sesji jest pusty!!");
            }
            
            _id = Guid.NewGuid().ToString();
            _serviceName = serviceName;
            _sessionID = sessionId;
            _editable = true;
        }
        public string GetContent() => _content ?? "";
        public string GetServiceName() => _serviceName;
        public string GetSessionID() => _sessionID;

        public void SetContent(string message)
        {
            if (_editable) _content = message;
        }

        public string GetID() =>_id;

        public string GetChecksum()
        {
            if (_checksum != null)
                return _checksum;
            SetChecksum();
            return _checksum;
        }

        public bool IsEditable => _editable;

        /// <summary>
        /// Sprawdza czy podana nazwa serwisu odpowiada dostępnym serwisom.
        /// </summary>
        /// <param name="serviceName">Nazwa serwisu.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private bool ServiceNameIsValid(string serviceName)
        {
            var enumValues = Enum.GetValues(typeof(ServicesEnum));
            foreach (var service in (ServicesEnum[]) enumValues)
            {
                if (service.ToString("F").Equals(serviceName) || serviceName.Equals("init"))
                {
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// Sprawdza czy wprowadzony identyfikator odpowiada jakiejkolwiek aktywnej sesji.
        /// </summary>
        /// <param name="sessionId">Wprowadzony identyfikator sesji.</param>
        /// <returns>Rezultat czy sesja o podanym identyfikatorze istnieje.</returns>
        private bool SessionIsValid(string sessionId)
        {
            return SessionManager.Instance.IsSessionExist(sessionId);
        }
        
        /// <summary>
        /// Wyznacza sumę kontrolną wiadomości jednocześnie zamykając ją na edycję.
        /// </summary>
        /// <exception cref="Exception">Wyjątek rzucany jeśli wymagane dane wiadomości nie są ustawione.</exception>
        public void SetChecksum()
        {
            if (!_editable) 
                Console.WriteLine("Suma kontrolna jest już wyznaczona, a wiadomość jest już nieedytowalna!!");
            if (_id == null || _sessionID == null || _serviceName == null)
                throw new Exception("Dane wiadomości są niepełne!!");
            
            StringBuilder builder = new StringBuilder();
            builder.Append(_id);
            builder.Append(_sessionID);
            builder.Append(_serviceName);
            builder.Append(_content);
            
            string messageContent = builder.ToString();
            _checksum = CryptographyModule.GetMd5Hash(messageContent);
            _editable = false;
        }

        /// <summary>
        /// Przygotowuje wiadomość do wysłania, zamieniając w pojedynczy string dodając separatory dla pól.
        /// </summary>
        /// <returns>Wiadomość w formie stringa gotowa do przekonwertowania do tablicy byte.</returns>
        public override string ToString()
        {
            StringBuilder convertedMessage = new StringBuilder();
            convertedMessage.Append(this.GetID()).Append(_separator);
            convertedMessage.Append(this.GetSessionID()).Append(_separator);
            convertedMessage.Append(this.GetServiceName()).Append(_separator);
            convertedMessage.Append(this.GetContent()).Append(_separator);
            convertedMessage.Append(this.GetChecksum()).Append(_separator).Append("\n");

            return convertedMessage.ToString();
        }
    }
}