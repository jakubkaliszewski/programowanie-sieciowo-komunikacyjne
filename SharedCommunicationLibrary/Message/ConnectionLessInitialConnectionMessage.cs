using System;
using System.Text;

namespace SharedCommunicationLibrary.Message
{
    public class ConnectionLessInitialConnectionMessage : InitialConnectionMessage
    {
        /// <summary>
        /// Tworzy początkową wiadomość dla UDP. Wykorzystywane przez serwer.
        /// Serwer podaje klientowi numer portu na, którym utworzył gniazdo do komunikacji z nim.
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="portNumber"></param>
        public ConnectionLessInitialConnectionMessage(string sessionId, string portNumber) : base(sessionId)
        {
            _sessionID = sessionId ?? throw new Exception("ID sesji jest pusty!!");
            _id = Guid.NewGuid().ToString();
            _content = portNumber;
            _serviceName = "init";
        }
        
        /// <summary>
        /// Tworzy początkową wiadomość dla UDP. Wykorzystywane przez klienta, gdyż podaje swój adres IP wraz z portem.
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="ipAddress"></param>
        /// <param name="portNumber"></param>
        public ConnectionLessInitialConnectionMessage(string sessionId, string ipAddress, string portNumber) : base(sessionId)
        {
            _sessionID = sessionId ?? throw new Exception("ID sesji jest pusty!!");
            _id = Guid.NewGuid().ToString();
            _content = ipAddress + "|" + portNumber;
            _serviceName = "init";
        }

        public override string ToString()
        {
            StringBuilder convertedMessage = new StringBuilder();
            convertedMessage.Append(this.GetID()).Append(_separator);
            convertedMessage.Append(this.GetSessionID()).Append(_separator);
            convertedMessage.Append(this.GetServiceName()).Append(_separator);
            convertedMessage.Append(this.GetContent()).Append(_separator);
            convertedMessage.Append(this.GetChecksum()).Append(_separator).Append("\n");

            return convertedMessage.ToString();
        }
    }
}