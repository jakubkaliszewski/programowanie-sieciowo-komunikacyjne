using System;
using System.IO.Ports;
using SharedCommunicationLibrary.Message;

namespace SharedCommunicationLibrary.Medium
{
    /// <summary>
    /// Medium komunikacjyjne dla portów szeregowych.
    /// </summary>
    public class RS232Medium : AbstractMedium
    {
        private SerialPort _serialPort;

        public RS232Medium(string destinationPortName) : base(destinationPortName)
        {
            _serialPort = new SerialPort(destinationPortName);
            // Set the read/write timeouts
            _serialPort.ReadTimeout = 500;
            _serialPort.WriteTimeout = 500;
            _serialPort.DtrEnable = true;
            _mediumType = MediumType.RS232;
        }
        /// <summary>
        /// Drugi konstruktor na "media" serwera, przecież to ten sam port będzie co listenera
        /// </summary>
        /// <param name="serialPort"></param>
        /// <param name="destinationPortName"></param>
        public RS232Medium(SerialPort serialPort, string destinationPortName) : base(destinationPortName)
        {
            _serialPort = serialPort;
            _mediumType = MediumType.RS232;
        }
        
        public override bool ConnectTo(string destinationPort, string destinationAddress)
        {
            _serialPort.Open();
            _connected = true;
            try
            {
                if (_connected)
                {
                    Console.WriteLine($"Nawiązano połączenie z {destinationPort}");
                    //wysłać init wiadomość na serwer
                    var bufferService = new BufferService();
                    var initialMessage = new ConnectionLessInitialConnectionMessage(
                        "",
                        "",
                        ""
                    );
                    
                    var bytes = BufferService.PrepareMessageToSend(initialMessage);
                    _serialPort.Write(bytes, 0, bytes.Length);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return _connected;
        }

        public override bool Disconnect()
        {
            if (_connected)
            {
                _serialPort.Close();
                _connected = false;
                return true;
            }

            return false;
        }

        public override string ToString()
        {
            if (_connected)
                return $"Połączenie SerialPort z {_destinationPortName}.";
            
            return "Brak połączenia SerialPort.";
        }

        public override void Dispose()
        {
            _serialPort.Dispose();
        }

        public override void SendMessage(Message.Message message)
        {
            var bytes = BufferService.PrepareMessageToSend(message);
            _serialPort.Write(bytes, 0, bytes.Length);
        }

        public override void BeginReceive()
        {
            CleanBuffer();
            _serialPort.DataReceived += new SerialDataReceivedEventHandler(PortDataReceived);
        }
        private void PortDataReceived(object sender, SerialDataReceivedEventArgs args)
        {
            _serialPort.DataReceived -= new SerialDataReceivedEventHandler(PortDataReceived);

            int bytesRead = _serialPort.Read(_buffer, 0, _bufferSize);
            if(bytesRead > 0)
                BufferService.Read(_buffer, this);
        }

        protected override void ReceiveCallback(IAsyncResult asyncResult)
        {
            throw new NotImplementedException();
        }


    }
}