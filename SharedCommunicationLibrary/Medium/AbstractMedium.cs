using System;

namespace SharedCommunicationLibrary.Medium
{
    public abstract class AbstractMedium
    {
        protected int _timeout = 5000;
        protected string _name;
        protected string _destinationPortName;
        protected bool _connected;
        protected MediumType _mediumType;
        protected int _bufferSize = 2048;
        protected byte[] _buffer;
        protected BufferService BufferService;

        protected AbstractMedium(string destinationPortName)
        {
            _destinationPortName = destinationPortName;
            _buffer = new byte[_bufferSize];
            BufferService = new BufferService();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns>Zwraca nazwę Medium</returns>
        public string GetName() => _name;
        /// <summary>
        /// 
        /// </summary>
        /// <returns>Zwraca informację o wykorzystywanym porcie przez Medium</returns>
        public string GetPortName() => _destinationPortName;
        /// <summary>
        /// 
        /// </summary>
        /// <returns>Zwraca informację o stanie połączenia.</returns>
        public bool IsConnected() => _connected;
        public MediumType GetMediumType() => _mediumType;
        /// <summary>
        /// Metoda wykonująca połączenie przez Medium. Wykorzystywana przez klienta w celu nawiązania połączenia z serwerem.
        /// </summary>
        /// <param name="portName">Domyślnie numer portu z którym chcesz się połączyć (UDP,TCP), lecz możliwa jest także nazwa słowna (RS-232).</param>
        /// <returns>Zwraca informację o powiedzeniu się akcji.</returns>
        public abstract bool ConnectTo(string destinationPort, string destinationAddress);
        /// <summary>
        /// Metoda wykonująca rozłączenie przez Medium.
        /// </summary>
        /// <returns>Zwraca informację o powiedzeniu się akcji.</returns>
        public abstract bool Disconnect();
        public abstract void Dispose();
        public abstract void SendMessage(Message.Message message);
        public abstract void BeginReceive();
        protected abstract void ReceiveCallback(IAsyncResult asyncResult);

        protected void CleanBuffer()
        {
            for (int i = 0; i < _bufferSize; i++)
            {
                _buffer[i] = 0;
            }
        }
    }
}