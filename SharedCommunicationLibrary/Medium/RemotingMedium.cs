using System;

namespace SharedCommunicationLibrary.Medium
{
    public class RemotingMedium : AbstractMedium
    {
        
        public RemotingMedium(string destinationPortName) : base(destinationPortName)
        {
            _mediumType = MediumType.REMOTING;
        }
        public override bool ConnectTo(string destinationPort, string destinationAddress)
        {
            throw new System.NotImplementedException();
        }

        public override bool Disconnect()
        {
            throw new System.NotImplementedException();
        }

        public override string ToString()
        {
            throw new System.NotImplementedException();
        }

        public override void Dispose()
        {
            throw new System.NotImplementedException();
        }

        public override void SendMessage(Message.Message message)
        {
            throw new System.NotImplementedException();
        }

        public override void BeginReceive()
        {
            throw new NotImplementedException();
        }

        protected override void ReceiveCallback(IAsyncResult asyncResult)
        {
            throw new NotImplementedException();
        }


    }
}