using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using SharedCommunicationLibrary.Message;

namespace SharedCommunicationLibrary.Medium
{
    /// <summary>
    /// Medium komunikacjyjne protokołu UDP
    /// </summary>
    public class UDPMedium : AbstractMedium
    {
        private Socket _udpSocket;
        private string _destinationAddress;
        private string _destinationPort;
        private EndPoint _remoteEP;

        public UDPMedium(string destinationPortName) : base(destinationPortName)
        {
            //ważne by uaktywnić dane urządzenie by było w stanie odebrać żądanie
            _udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _udpSocket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), Int32.Parse(ConfigurationManager.AppSettings["UDPPort"])));//przypisanie do gniazda ip 127.0.0.1 oraz portu
            _connected = false;
            _mediumType = MediumType.UDP;
        }
        
        public UDPMedium(Socket udpSocket, string destinationPortName, EndPoint remoteEP) : base(destinationPortName)
        {
            _udpSocket = udpSocket;
            _connected = udpSocket.Connected;
            _mediumType = MediumType.UDP;
            _remoteEP = remoteEP;
        }
        
        public override bool ConnectTo(string destinationPort, string destinationAddress)
        {
            try
            {
                Console.WriteLine($"Nawiązywanie połączenia UDP z {destinationAddress}:{destinationPort}...");
                // Połączenie używając limitu czasu oczekiwania na jego nawiązanie
                _remoteEP = new IPEndPoint(IPAddress.Parse(destinationAddress), Int32.Parse(destinationPort));

                int counter = 0;
                while (!_connected && counter < _timeout)
                {
                    _udpSocket.BeginConnect(destinationAddress, Int32.Parse(destinationPort), null, null);
                    counter+=2000;
                    Thread.Sleep(2000);
                    _connected = _udpSocket.Connected;
                }
                
                if (_connected)
                {
                    Console.WriteLine($"Nawiązano połączenie z {destinationAddress}:{destinationPort}");
                    //wysłać init wiadomość na serwer
                    var bufferService = new BufferService();
                    var localEndPoint = (IPEndPoint) (_udpSocket.LocalEndPoint);
                    var initialMessage = new ConnectionLessInitialConnectionMessage(
                        "",
                        localEndPoint.Address.ToString(),
                        localEndPoint.Port.ToString()
                        );
                    _udpSocket.SendTo(
                        bufferService.PrepareMessageToSend(initialMessage), 
                        SocketFlags.None,
                        _remoteEP
                        );
                    
                    //nasłuchiwanie włączyć
                    //otrzymać wiadomość
                    if(_udpSocket.ReceiveFrom(_buffer, ref _remoteEP) > 0)
                    {
                        string data = Encoding.ASCII.GetString(_buffer);
                        var messagesInBuffer = data.Split('\n').ToList();

                        var splited = messagesInBuffer[0].Split(';');
                        if (splited.Length != 6)
                            throw new Exception("Otrzymano niepełne dane od Klienta!");

                        string messageID = splited[0];
                        string sessionID = splited[1];
                        string serviceName = splited[2];
                        string content = splited[3];
                        string checkSum = splited[4];

                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append(messageID);
                        stringBuilder.Append(sessionID);
                        stringBuilder.Append(serviceName);
                        stringBuilder.Append(content);

                        if (!CryptographyModule.VerifyMd5Hash(stringBuilder.ToString(), checkSum))
                            throw new CryptographicException(
                                "Suma kontrolna nie zgadza się z zadeklarowaną sumą kontrolną otrzymaną w wiadomości!!");

                        string remotePort = content;
                        _remoteEP = new IPEndPoint(IPAddress.Parse(destinationAddress), Int32.Parse(remotePort));
                        _destinationAddress = destinationAddress;
                        _destinationPort = remotePort;
                        _udpSocket.Connect(_remoteEP);
                    }
                }
                else
                {
                    _udpSocket.Close();
                    throw new TimeoutException($"Przekroczenie czasu połączenia z serwerem {destinationAddress}:{destinationPort}.");
                }
            }
            catch (TimeoutException exception)
            {
                throw new Exception($"Błąd połączenia z serwerem {destinationAddress}:{destinationPort}.", exception);
            }

            return _connected;
        }

        public override bool Disconnect()
        {
            if (_connected)
            {
                _udpSocket.Disconnect(false);
                _connected = false;
                return true;
            }

            return false;
        }

        public override string ToString()
        {
            if (_connected)
                return $"Połączenie UDP z {_destinationAddress}:{_destinationPort}.";
            
            return "Brak połączenia UDP.";
        }

        public override void Dispose()
        {
            _udpSocket.Dispose();
        }

        public override void SendMessage(Message.Message message)
        {
            var bytes = BufferService.PrepareMessageToSend(message);
            _udpSocket.SendTo(bytes, _remoteEP);
        }

        public override void BeginReceive()
        {
            CleanBuffer();
            try
            {
                _udpSocket.BeginReceiveFrom(_buffer, 0, _bufferSize, 0, ref _remoteEP, ReceiveCallback, this);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            //_udpSocket.BeginReceive(_buffer, 0, _bufferSize, 0, ReceiveCallback, this);
        }

        protected override void ReceiveCallback(IAsyncResult asyncResult)
        {
            // Retrieve the state object and the handler socket  
            // from the asynchronous state object.  
            UDPMedium state = (UDPMedium) asyncResult.AsyncState;  
            Socket handler = state._udpSocket;  
  
            // Read data from the client socket.   
            int bytesRead = handler.EndReceive(asyncResult);  
            if(bytesRead > 0)
                BufferService.Read(_buffer, this);
        }
    }
}