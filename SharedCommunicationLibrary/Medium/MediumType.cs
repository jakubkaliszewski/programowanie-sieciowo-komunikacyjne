using System.ComponentModel;

namespace SharedCommunicationLibrary.Medium
{
    /// <summary>
    /// Enum posiadający wszystkie możliwe typy oferowanych połączeń.
    /// </summary>
    public enum MediumType
    {
        [Description("TCP Connection")]
        TCP,
        [Description("UDP Connection")]
        UDP,
        [Description("RS-232 Serial Connection")]
        RS232,
        [Description(".NET Remoting Connection")]
        REMOTING,
        [Description("Communication via the file on the Server")]
        FILE,
    }
    
}