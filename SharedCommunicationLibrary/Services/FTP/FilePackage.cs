using System;

namespace SharedCommunicationLibrary.Services.FTP
{
    public class FilePackage
    {
        private int _packageId;
        private string _data;

        public FilePackage(int packageId, string data)
        {
            _packageId = packageId;
            _data = data ?? throw new ArgumentNullException(nameof(data));
        }

        public int PackageId => _packageId;

        public string Data => _data;
    }
}