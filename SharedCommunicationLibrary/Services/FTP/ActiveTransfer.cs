using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Timer = System.Timers.Timer;

namespace SharedCommunicationLibrary.Services.FTP
{
    public abstract class ActiveTransfer
    {
        protected Guid _id;
        protected Stream _stream;
        protected int _lastConfirmedPackage;
        protected LinkedList<FilePackage> _unconfirmedPackages;
        protected bool _endFlag;
        protected string _sessionId;
        protected string _resourceName;

        public LinkedList<FilePackage> UnconfirmedPackages => _unconfirmedPackages;

        public string SessionId => _sessionId;

        public string ResourceName => _resourceName;

        public Guid Id => _id;

        protected ActiveTransfer(Guid id, string sessionId, string resourceName) //przy otrzymaniu zlecenia, dla serwera, bo on nigdy id Transferu nie ustali
        {
            _id = id;
            _unconfirmedPackages = new LinkedList<FilePackage>();
            _sessionId = sessionId;
            _resourceName = resourceName;
        }

        protected ActiveTransfer(string sessionId, string resourceName) //inicjacja po twojej stronie
        {
            _id = Guid.NewGuid();
            _unconfirmedPackages = new LinkedList<FilePackage>();
            _sessionId = sessionId;
            _resourceName = resourceName;
        }



        protected void AddUnconfirmedPackage(FilePackage package)
        {
            bool added = false;
            for (var recentNode = _unconfirmedPackages.First; recentNode != null; recentNode = recentNode.Next)
            {
                if (recentNode.Value.PackageId > package.PackageId)
                {
                    _unconfirmedPackages.AddBefore(recentNode, package);
                    added = true;
                }
            }

            if (!added)
                _unconfirmedPackages.AddLast(package);
        }

        public void ConfirmPackage(string packageId)
        {
            for (var recentNode = _unconfirmedPackages.First; recentNode != null; recentNode = recentNode.Next)
            {
                if (recentNode.Value.PackageId.Equals(packageId))
                {
                    _unconfirmedPackages.Remove(recentNode);
                    break;
                }
            }
        }

        protected FilePackage GetFirstFilePackage()
        {
            return _unconfirmedPackages.First.Value;
        }

        public void CloseTransfer(int lastPackageId)
        {
            _endFlag = true;
            //usypianie wątku sprawdzającego, by w tym czasie przyszły brakujące paczki, ponowione przez nadawcę.
            while(_unconfirmedPackages.Count != 0 && _lastConfirmedPackage != lastPackageId)
                Thread.Sleep(1000);

            CloseStream();
        }

        protected void CloseStream()
        {
            _stream.Close();
        }
    }
}