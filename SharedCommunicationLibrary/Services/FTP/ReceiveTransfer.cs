using System;
using System.IO;
using System.Text;
using System.Timers;

namespace SharedCommunicationLibrary.Services.FTP
{
    public class ReceiveTransfer : ActiveTransfer
    {
        public ReceiveTransfer(Guid id, string fileName, string sessionId, string resourceName) : base(id, sessionId, resourceName)
        {
            _stream = new FileStream(fileName, FileMode.CreateNew, FileAccess.Write); //zapisywanie pliku od klienta (send)
        }

        public ReceiveTransfer(string sessionId, string resourceName) : base(sessionId, resourceName)//inicjalizacja get u klienta, nie zna nazwy zapisywanego pliku jeszcze...
        {
        }
        
        public void SetFileStream(string fileName)//przy get, wychodzące od klienta, nie znam nazwy pliku otrzymasz w pierwszej wiadomości od serwera
        {
            if (_stream != null)
                return;
            _stream = new FileStream(fileName, FileMode.CreateNew, FileAccess.Write);
        }
        
        /// <summary>
        /// Dodaje otrzymaną w wiadomości paczkę danych do Streamu.
        /// </summary>
        /// <param name="package">Paczka danych</param>
        public void AddNewPackageToStream(FilePackage package)
        {
            //sprawdzić czy paczka jest następnikiem ostatnio dodanej do streama
            if (package.PackageId == _lastConfirmedPackage + 1)
            {
                AddPackageToStream(package);
                _lastConfirmedPackage = package.PackageId;
                
                //a także sprawdź czy pierwsza na liście pozycja nie jest następnikiem po id, rób tak aż do złamania warunku.
                while (_lastConfirmedPackage + 1 == GetFirstFilePackage().PackageId)
                {
                    var actualPackage = GetFirstFilePackage();
                    AddPackageToStream(actualPackage);
                    _unconfirmedPackages.RemoveFirst();
                }
            }
            else
                AddUnconfirmedPackage(package);
        }

        /// <summary>
        /// Zapisuje paczkę danych do pliku przy pomocy Streama.
        /// </summary>
        /// <param name="package">Paczka danych.</param>
        private void AddPackageToStream(FilePackage package)
        {
            byte[] dataToSave = new ASCIIEncoding().GetBytes(package.Data);
            _stream.Write(dataToSave, 0, dataToSave.Length);
        }
    }
}