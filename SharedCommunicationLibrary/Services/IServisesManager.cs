namespace SharedCommunicationLibrary.Services
{
    public interface IServisesManager
    {
        void DelegateMessageToService(Message.Message message);
        //ma zadanie wywołać odpowiedni serwis na podstawie otrzymanej wiadomości
        //dwie wersje: dla serwera oraz klienta.
        //serwer: jego serwisy są inne, realizują pewne zadania...
        //klient: jego serwisy, głównie skupiają się na odczytaniu wiadomości i jej interpretacji

        //odpowiedzi zwrotne są realizowane za pomocą menedżera w sesji, korzystają z tego serwisy...
        //Jeśli serwis odpowiada na wiadomość to ID Sesji wyciąga z wiadomości...wyszukując potem odpowiednią sesję z pomocą menedżera.
        
        
    }
}

