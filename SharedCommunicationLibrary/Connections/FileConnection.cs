using SharedCommunicationLibrary.Medium;

namespace SharedCommunicationLibrary.Connections
{
    public class FileConnection : Connection
    {
        public FileConnection(string port, string address, AbstractMedium medium) : base(port, address, medium)
        {
        }
    }
}