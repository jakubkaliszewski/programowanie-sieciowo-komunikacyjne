using SharedCommunicationLibrary.Medium;

namespace SharedCommunicationLibrary.Connections
{
    public class RS232Connection : Connection
    {
        public RS232Connection(string port, string address, AbstractMedium medium) : base(port, address, medium)
        {
        }
    }
}