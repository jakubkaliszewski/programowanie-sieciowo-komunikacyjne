﻿using System;
using System.ComponentModel;
using System.Threading;
using SharedCommunicationLibrary;
using SharedCommunicationLibrary.Medium;
using SharedCommunicationLibrary.Services;
using SharedCommunicationLibrary.Session;
using Container = SimpleInjector.Container;

namespace CommunicationClient
{
    internal class Program
    {
        public static string SessionId;
        public static void Main()
        {
            SetDependencyInjectionConfig();

            while (true)
            {
                try
                {
                    MediumType? choice = ChooseMedium();
                    if (choice == null)
                        throw new Exception("Wybór niepoprawny!");

                    Console.WriteLine($"Wybrano medium {choice}.");
                    //Połączenie inicjujące uzyskanie sesji i połączenie się z serwerem
                    CreateInitialConnection((MediumType) choice);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                while (SessionId == null)
                {
                    Thread.Sleep(200);
                }
                ConsoleInterpreter.Start(SessionManager.Instance.GetSessionByID(SessionId));
            }
        }

        /// <summary>
        /// Zawiera ustawienia dot wstrzykiwania zależności dla klienta.
        /// Tu także rejestruje się nowe rozwiązania w kontenerze.
        /// </summary>
        private static void SetDependencyInjectionConfig()
        {
            var container = new Container();
            // 2. Configure the container (register)
            container.Register<IServisesManager, ServisesManager>();
            // 3. Verify your configuration
            container.Verify();
            DependencyInjection.SetRegisteredContainer(container);
        }

        private static void CreateInitialConnection(MediumType mediumType)
        {
            InitialSessionFactory factory = new InitialSessionFactory();
            factory.CreateSession(mediumType);
        }

        private static MediumType? ChooseMedium()
        {
            Console.WriteLine("Wybierz medium, które chcesz wykorzystać (Numer):");

            int numberChoice;
            var enumValues = Enum.GetValues(typeof(MediumType));
            foreach (MediumType medium in (MediumType[]) enumValues)
            {
                Console.WriteLine($"({(int) medium}). {GetCustomDescription(medium)}");
            }

            Console.Write("Wybór: ");
            numberChoice = Int32.Parse(Console.ReadLine());

            for (int i = 0; i <= enumValues.Length; i++)
            {
                if (numberChoice == i)
                    return (MediumType) enumValues.GetValue(i);
            }

            return null;
        }

        private static string GetCustomDescription(object objEnum)
        {
            var field = objEnum.GetType().GetField(objEnum.ToString());
            var attributes = (DescriptionAttribute[]) field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description.ToLower() : objEnum.ToString().ToLower();
        }

        private static string Description(Enum value)
        {
            return GetCustomDescription(value);
        }
    }
}