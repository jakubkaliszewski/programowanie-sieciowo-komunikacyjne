using System;
using System.Configuration;
using System.Threading;
using SharedCommunicationLibrary;
using SharedCommunicationLibrary.Connections;
using SharedCommunicationLibrary.Medium;
using SharedCommunicationLibrary.Session;

namespace CommunicationClient
{
    public class InitialSessionFactory : ConnectionFactory
    {
        private static Connection connection;
        public void CreateSession(MediumType mediumType)
        {
            string destinationPort, destinationAddress = null;
            switch (mediumType)
            {
                case MediumType.TCP:
                {
                    destinationPort = ConfigurationManager.AppSettings["TCPListenerPort"];
                    destinationAddress = ConfigurationManager.AppSettings["ServerIP"];
                    break;
                }
                case MediumType.UDP:
                {
                    destinationPort = ConfigurationManager.AppSettings["UDPListenerPort"];
                    destinationAddress = ConfigurationManager.AppSettings["ServerIP"];
                    break;
                }
                case MediumType.RS232:
                {
                    destinationPort = ConfigurationManager.AppSettings["RS232Port"];
                    break;
                }
                case MediumType.REMOTING:
                {
                    destinationPort = ConfigurationManager.AppSettings["RemotingListenerPort"];
                    destinationAddress = ConfigurationManager.AppSettings["ServerIP"];
                    break;
                }
                case MediumType.FILE:
                {
                    destinationPort = ConfigurationManager.AppSettings["DirPath"] + Guid.NewGuid().ToString() + ".in";//katalog serwera + Guid.in
                    break;
                }
                default:
                    throw new ArgumentException();
            }

            connection = InitializeConnection(mediumType, destinationPort, destinationAddress);
            connection.Medium.BeginReceive();

            while (SessionManager.Instance.GetSessionCount() == 0)//jakieś czekanie aktywne...
            {
                Thread.Sleep(200);
            }
        }

        public static void SetSession(string sessionId)
        {
            if (sessionId != null && connection != null)
            {
                Program.SessionId = sessionId;
                SessionManager.Instance.AddSession(new Session(connection, sessionId));
            }
        }
    }
}