using System;
using CommunicationClient.Services;
using SharedCommunicationLibrary.Message;
using SharedCommunicationLibrary.Services;

namespace CommunicationClient
{
    public class ServisesManager : IServisesManager//używane w trakcie odbierania bardziej
    {
        public void DelegateMessageToService(Message message)
        {
            var serviceName = message.GetServiceName();
            if (serviceName.Equals("init"))
            {
                SetSession(message);
            }
            
            serviceName = serviceName.ToUpper();
            Enum.TryParse(serviceName, out ServicesEnum serviceFromMessage);
            
            switch (serviceFromMessage)
            {
                case ServicesEnum.PING:
                {
                    var ping = PingService.Instance;
                    if (ping.IsActive())
                        ping.DoAction(message);
                    break;
                } 
                case ServicesEnum.COMMUNICATOR:
                {
                    var chat = CommunicatorService.Instance;
                    if (chat.IsActive())
                        chat.DoAction(message);

                    break;
                }
                case ServicesEnum.FTP_SERVICE:
                {
                    var config = FTPService.Instance;
                    if(config.IsActive())
                        config.DoAction(message);

                    break;
                } 
                case ServicesEnum.CONFIG:
                {
                    var config = ConfigService.Instance;
                    if(config.IsActive())
                        config.DoAction(message);

                    break;
                }
                default:
                    throw new ArgumentException("Odczytany z wiadomości typ serwisu nie istnieje!!");
            }
        }

        private static void SetSession(Message message)
        {
            InitialSessionFactory.SetSession(message.GetSessionID());
        }
        
        
    }
}