using System;
using System.Globalization;
using System.Text;
using SharedCommunicationLibrary;
using SharedCommunicationLibrary.Message;
using SharedCommunicationLibrary.Services;
using SharedCommunicationLibrary.Session;

namespace CommunicationClient.Services
{
    /*Schemat treści wiadomości dla komunikatora
- Separator: '|'
- login adresata,
- treść właściwa,
- login nadawcy
- czas otrzymania
*/
    /// <summary>
    /// Serwis COMMUNICATOR po stronie klienta.
    /// </summary>
    public sealed class CommunicatorService : IService
    {
        private static readonly CommunicatorService _communicatorServiceInstance = new CommunicatorService();
        private bool _active = true;
        private readonly char _separator = '|';
        static CommunicatorService(){}
        public static CommunicatorService Instance
        {
            get { return _communicatorServiceInstance; }
        }
        /// <summary>
        /// Metoda odbierająca wiadomość dla serwisu i wyświetlająca wiadomość na ekranie.
        /// </summary>
        /// <param name="message">Odebrana wiadomość.</param>
        public void DoAction(Message message)
        {
            string senderLogin = GetSenderLoginFromMessage(message.GetContent());
            string messageContent = GetContentFromMessage(message.GetContent());
            DateTime dateTime = GetDataTimeFromMessage(message.GetContent());
            Console.WriteLine();
            Console.WriteLine($"Otrzymano wiadomość od {senderLogin}!\n{messageContent}\n{dateTime.ToLocalTime().ToString("F", CultureInfo.CreateSpecificCulture("pl-PL"))}");
            Console.WriteLine();
            Console.Beep(1000, 1000);
        }

        public void Start()
        {
            _active = true;
        }

        public void Stop()
        {
            _active = true;
        }

        public bool IsActive()
        {
            return _active;
        }

        /// <summary>
        /// Metoda wysyłająca wiadomość COMMUNICATOR do serwera.
        /// </summary>
        /// <param name="message">Wiadomość do przygotowania w ramach serwisu COMMUNICATOR.</param>
        /// <param name="oryginalCommand">Nie przetworzone wejście z linii poleceń.</param>
        /// <param name="login">Login użytkownika</param>
        /// <exception cref="Exception">Wyjątek rzucany w przypadku zbyt małej ilości argumentów.</exception>
        public void DoAction(Message message, string oryginalCommand, string login)
        {
            var splitted = oryginalCommand.Split('"');
            if (splitted.Length != 3)
            {
                splitted =  oryginalCommand.Split(' ');
                if(splitted.Length != 2)
                    throw new Exception("Wprowadzono za mało argumentów by wysłać wiadomość!");
            }
               
            string messageContent = splitted[1];
            string receiver = null;
            if(!messageContent.StartsWith("show"))
                receiver = splitted[2].Trim();
            
            PrepareCommunicatorMessage(message, login, receiver, messageContent);
            SendResponse(message.GetSessionID(), message);
        }

        /// <summary>
        /// Dostarcza z wiadomości komunikatora login nadawcy.
        /// </summary>
        /// <param name="message">Wiadomość dostarczona na serwer.</param>
        /// <returns>Login nadawcy wiadomości.</returns>
        private string GetSenderLoginFromMessage(string message)
        {
            return SplitContentMessage(message)[2];
        }

        /// <summary>
        /// Dostarcza z wiadomości komunikatora przekazywaną treść.
        /// </summary>
        /// <param name="message">Wiadomość dostarczona na serwer.</param>
        /// <returns>Treść wiadomości.</returns>
        private string GetContentFromMessage(string message)
        {
            return SplitContentMessage(message)[1];
        }
        
        /// <summary>
        /// Dostarcza z wiadomości komunikatora przekazywaną treść.
        /// </summary>
        /// <param name="message">Wiadomość dostarczona na serwer.</param>
        /// <returns>Treść wiadomości.</returns>
        private DateTime GetDataTimeFromMessage(string message)
        {
            string datatimeString = SplitContentMessage(message)[3];
            DateTime dateTime = DateTime.SpecifyKind(DateTime.Parse(datatimeString,CultureInfo.CreateSpecificCulture("pl-PL")), DateTimeKind.Utc);
            return dateTime;
        }

        /// <summary>
        /// Rozdziela zawartość wiadomości wydobywając z niej parametry konwersacji.
        /// </summary>
        /// <param name="message">Wiadomość dostarczona na serwer.</param>
        /// <returns>Podzielona zawartość wiadomości.</returns>
        private string[] SplitContentMessage(string message)
        {
            var splited = message.Split(_separator);
            if (splited.Length != 5)
                throw new Exception("Wiadomość komunikatora nie zawiera wszystkich pól!!");
            return splited;
        }

        /// <summary>
        /// Otrzymanie instancji sesji na podstawie jej ID.
        /// </summary>
        /// <param name="sessionID">ID sesji.</param>
        /// <returns>Instancja sesji, która posiada podane ID.</returns>
        private Session GetSessionByID(string sessionID)
        {
            var sessionManager = SessionManager.Instance;
            return sessionManager.GetSessionByID(sessionID);
        }

        /// <summary>
        /// Zlecenie wysłania wiadomości - odpowiedzi w ramach podanej sesji.
        /// </summary>
        /// <param name="sessionID">Identyfikator sesji</param>
        /// <param name="messageToResponse">Wysyłana wiadomość</param>
        private void SendResponse(string sessionID, Message messageToResponse)
        {
            var sessionManager = SessionManager.Instance;
            sessionManager.SendMessage(sessionID, messageToResponse);
        }
        /// <summary>
        /// Przygotowuje wiadomość dla usługi Komunikatora.
        /// </summary>
        /// <param name="message">Wiadomość do przygotowania</param>
        /// <returns>Przygotowana wiadomość.</returns>
        private void PrepareCommunicatorMessage(Message message, string login, string receiver, string messageContent)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (receiver != null)
            {
                stringBuilder.Append(receiver).Append(_separator);
            }
            stringBuilder.Append(messageContent).Append(_separator);
            stringBuilder.Append(login).Append(_separator);
            
            message.SetContent(stringBuilder.ToString());
            message.SetChecksum();
        }
    }
}