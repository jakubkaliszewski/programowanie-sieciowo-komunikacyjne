using System;
using SharedCommunicationLibrary;
using SharedCommunicationLibrary.Message;
using SharedCommunicationLibrary.Services;
using SharedCommunicationLibrary.Session;

namespace CommunicationClient.Services
{
    /// <summary>
    /// Serwis PING po stronie klienta.
    /// </summary>
    public sealed class PingService : IService
    {
        private static readonly PingService _pingServiceInstance = new PingService();
        private bool _active = true;
        private DateTime _sendDateTime;

        static PingService()
        {
        }

        public static PingService Instance
        {
            get { return _pingServiceInstance; }
        }

        /// <summary>
        /// Metoda odbierająca wiadomość dla serwisu i wyświetlająca rezultat na ekranie.
        /// </summary>
        /// <param name="message">Odebrana wiadomość.</param>
        public void DoAction(Message message) //porównuję czasy przy pingu.
        {
            var resultInMiliseconds = Math.Abs(DateTime.UtcNow.Millisecond - _sendDateTime.Millisecond);
            var destination = GetSessionByID(message.GetSessionID()).Connection.DestinationAddress;
            Console.WriteLine($"PING do {destination} w czasie: {resultInMiliseconds} ms");
        }

        public void Start()
        {
            _active = true;
        }

        public void Stop()
        {
            _active = true;
        }

        public bool IsActive()
        {
            return _active;
        }

        /// <summary>
        /// Metoda wysyłająca wiadomość PING do serwera.
        /// </summary>
        /// <param name="message">Wiadomość do przygotowania w ramach serwisu PING.</param>
        /// <param name="parameters">Parametry wprowadzone w konsoli, do interpretacji i walidacji.</param>
        public void DoAction(Message message, string[] parameters)
        {
            var messageToSend = PreparePingMessage(message);
            SendResponse(messageToSend.GetSessionID(), message);
        }

        /// <summary>
        /// Otrzymanie instancji sesji na podstawie jej ID.
        /// </summary>
        /// <param name="sessionID">ID sesji.</param>
        /// <returns>Instancja sesji, która posiada podane ID.</returns>
        private Session GetSessionByID(string sessionID)
        {
            var sessionManager = SessionManager.Instance;
            return sessionManager.GetSessionByID(sessionID);
        }

        /// <summary>
        /// Zlecenie wysłania wiadomości - odpowiedzi w ramach podanej sesji.
        /// </summary>
        /// <param name="sessionID">Identyfikator sesji</param>
        /// <param name="messageToResponse">Wysyłana wiadomość</param>
        private void SendResponse(string sessionID, Message messageToResponse)
        {
            var sessionManager = SessionManager.Instance;
            _sendDateTime = DateTime.UtcNow;
            sessionManager.SendMessage(sessionID, messageToResponse);
        }

        /// <summary>
        /// Przygotowuje wiadomość dla usługi Pingu.
        /// </summary>
        /// <param name="message">Wiadomość do przygotowania</param>
        /// <returns>Przygotowana wiadomość</returns>
        private Message PreparePingMessage(Message message)
        {
            message.SetChecksum();
            return message;
        }
    }
}