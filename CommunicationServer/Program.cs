﻿using System.Threading.Tasks;
using CommunicationServer.Database;
using CommunicationServer.Listeners;
using SharedCommunicationLibrary;
using SharedCommunicationLibrary.Services;
using SimpleInjector;

namespace CommunicationServer
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            SetDependencyInjectionConfig();
            TCPListener tcpListener = new TCPListener();
            UDPListener udpListener = new UDPListener();

            new Task(() => tcpListener.Start(1)).Start();
            new Task(() => udpListener.Start(1)).Start();
            
            while (true)
            {
            }
        }
        
        /// <summary>
        /// Zawiera ustawienia dot wstrzykiwania zależności dla serwera.
        /// Tu także rejestruje się nowe rozwiązania w kontenerze.
        /// </summary>
        private static void SetDependencyInjectionConfig()
        {
            var container = new Container();
            // 2. Configure the container (register)
            container.Register<IServisesManager, ServisesManager>();
            container.Register<IDbContext, DbContext>();
            container.Register<IMessageRepository, MessageRepository>();
            container.Register<IFileRepository, FileRepository>();
            // 3. Verify your configuration
            container.Verify();
            DependencyInjection.SetRegisteredContainer(container);
        }
    }
}