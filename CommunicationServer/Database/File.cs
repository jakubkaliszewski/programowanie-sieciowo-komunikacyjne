namespace CommunicationServer.Database
{
    public class File
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public string FileName { get; set; }

        public File(string filename, string alias)
        {
            Alias = alias;
            FileName = filename;
        }
    }
}