using System.Collections.Generic;
using System.Linq;
using LiteDB;

namespace CommunicationServer.Database
{
    public class MessageRepository : IMessageRepository
    {
        private IDbContext _database;
        private readonly LiteCollection<MessageDb> _collection;
        public MessageRepository(IDbContext dbContext)
        {
            _database = dbContext;
            _collection = _database.GetDb().GetCollection<MessageDb>("messages");
            _collection.EnsureIndex(x => x.Login);
        }

        public void AddMessage(MessageDb message)
        {
            _collection.Insert(message);
        }

        public IList<MessageDb> GetMessagesByLogin(string login)
        {
            return _collection.Find(Query.EQ("Login", login)).ToList();
        }

        public void DeleteMessageByID(int id)
        {
            _collection.Delete(id);
        }

        public void DeleteMessages(IList<MessageDb> messagesToDelete)
        {
            var IDs = GetIDsFromMessages(messagesToDelete);
            foreach (var id in IDs)
            {
                DeleteMessageByID(id);
            }
        }

        private IList<int> GetIDsFromMessages(IList<MessageDb> messages)
        {
            return messages.Select(element => element.Id).ToList();
        }
    }
}