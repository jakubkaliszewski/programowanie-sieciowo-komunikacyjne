using LiteDB;

namespace CommunicationServer.Database
{
    public interface IDbContext
    {
        LiteDatabase GetDb();
    }
}