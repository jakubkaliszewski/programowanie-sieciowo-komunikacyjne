using LiteDB;

namespace CommunicationServer.Database
{
    public class FileRepository : IFileRepository
    {
        private IDbContext _database;
        private readonly LiteCollection<File> _collection;

        public FileRepository(IDbContext dbContext)
        {
            _database = dbContext;
            _collection = _database.GetDb().GetCollection<File>("files");
        }

        public void AddFile(File file)
        {
            _collection.Insert(file);
        }

        public void DeleteFile(File file)
        {
            _collection.Delete(file1 => file1.Id == file.Id);
        }

        public bool IsFileExist(string alias)
        {
            var result = _collection.FindOne(file => file.Alias.Equals(alias));
            if (result == null)
                return false;
            return true;
        }
        
        public File GetFileByAlias(string alias)
        {
            return _collection.FindOne(file => file.Alias.Equals(alias));
        }
    }
}