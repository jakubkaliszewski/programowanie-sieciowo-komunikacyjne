using System;

namespace CommunicationServer.Database
{
    /// <summary>
    /// Obiekt typu POCO. Odwzorowuje wiadomość zapisywaną w bazie danych.
    /// </summary>
    public class MessageDb
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Content { get; set; }
        private DateTime _dateTime;

        public DateTime DateTime {
            get => _dateTime.ToUniversalTime();
            set => _dateTime = value;
        }

        public string SenderLogin { get; set; }
    }
}