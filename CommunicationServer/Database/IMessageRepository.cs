using System.Collections.Generic;
using SharedCommunicationLibrary;

namespace CommunicationServer.Database
{
    public interface IMessageRepository
    {
        void AddMessage(MessageDb message);
        IList<MessageDb> GetMessagesByLogin(string login);
        void DeleteMessageByID(int id);
        void DeleteMessages(IList<MessageDb> messagesToDelete);
    }
}