﻿using System;
using System.Configuration;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SharedCommunicationLibrary;
using SharedCommunicationLibrary.Connections;
using SharedCommunicationLibrary.Medium;
using SharedCommunicationLibrary.Message;
using SharedCommunicationLibrary.Session;

namespace CommunicationServer.Listeners
{
    public sealed class RS232Listener : IListener
    {
        private static RS232Listener _listener = new RS232Listener();
        private static SerialPort _serialPort;
        private static string _port;
        private static bool _active;
        private byte[] _byteData = new byte[2048];

        static RS232Listener()
        {
            _port = ConfigurationManager.AppSettings["RS232Port"];
        }

        public static RS232Listener Instance
        {
            get { return _listener; }
        }

        public void Start(int backlog = 1)
        {
            _serialPort = new SerialPort(_port);
            _serialPort.ReadTimeout = 500;
            _serialPort.WriteTimeout = 500;
            _serialPort.DtrEnable = true;
            
            try
            {
                _active = true;
                _serialPort.Open();

                Console.WriteLine($"Oczekiwanie na połączenie Serial Port {_port}...");
                
                if (_active)
                {
                    _serialPort.DataReceived += new SerialDataReceivedEventHandler(PortDataReceived);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
        
        private void PortDataReceived(object sender, SerialDataReceivedEventArgs args)
        {
            _serialPort.DataReceived -= new SerialDataReceivedEventHandler(PortDataReceived);
            if (_active)
            {
                byte[] dataToMethod = new byte[_byteData.Length];
                int bytesRead = _serialPort.Read(dataToMethod, 0, dataToMethod.Length);
                if(bytesRead > 0)
                    new Task(()=>AcceptConnection(dataToMethod)).Start();
            }
        }

        private void AcceptConnection(byte[] buffer)
        {
            try
            {
                //klient wysłał swoją wiadomość przedstawiającą
                Console.WriteLine($"Połączono!");

                int size = buffer.Length;
                string data = Encoding.ASCII.GetString(buffer);
                if (data.Length != size)
                {
                    Console.WriteLine("Otrzymano niepełne dane od Klienta!");
                    return;
                }

                var messagesInBuffer = data.Split('\n').ToList();

                var splited = messagesInBuffer[0].Split(';');
                if (splited.Length != 6)
                    throw new Exception("Otrzymano niepełne dane od Klienta!");

                string messageID = splited[0];
                string sessionID = splited[1];
                string serviceName = splited[2];
                string content = splited[3];
                string checkSum = splited[4];

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(messageID);
                stringBuilder.Append(sessionID);
                stringBuilder.Append(serviceName);
                stringBuilder.Append(content);

                if (!CryptographyModule.VerifyMd5Hash(stringBuilder.ToString(), checkSum))
                    throw new CryptographicException(
                        "Suma kontrolna nie zgadza się z zadeklarowaną sumą kontrolną otrzymaną w wiadomości!!");

                if(!serviceName.Equals("init"))
                    throw new Exception("Nie otrzymano wiadomości init, zachęcającej do nawiązania sesji połączenia!!");



                var RSMedium = new RS232Medium(_serialPort, _port);
                var newConnection = new Connection(_port, "", RSMedium);

                var sessionManager = SessionManager.Instance;
                var session = sessionManager.CreateNewSession(newConnection);
                //wysłanie wiadomości inicjującej do klienta zawierającej id sesji
                var initialMessage = new InitialConnectionMessage(session.ID);
                initialMessage.SetChecksum();
                newConnection.SendMessage(initialMessage);
                Console.WriteLine("Wysłano wiadomość z sesją do klienta!");

                RSMedium.BeginReceive();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void Stop()
        {
            _active = false;
        }

        public bool IsActive()
        {
            return _active;
        }
    }
}
