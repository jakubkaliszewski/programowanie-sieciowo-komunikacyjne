﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using SharedCommunicationLibrary;
using SharedCommunicationLibrary.Connections;
using SharedCommunicationLibrary.Medium;
using SharedCommunicationLibrary.Message;
using SharedCommunicationLibrary.Session;

namespace CommunicationServer.Listeners
{
    public sealed class TCPListener : IListener
    {
        private static TCPListener _listener = new TCPListener();
        private static Socket _tcpSocket;
        private static int _port;
        private static bool _active;
        public static ManualResetEvent _allDone = new ManualResetEvent(false);  //Zdarzenie do sterowania wątkiem

        static TCPListener()
        {
            //ważne by uaktywnić dane urządzenie by było w stanie odebrać żądanie
            _port = Int32.Parse(ConfigurationManager.AppSettings["TCPPort"]);
        }

        public static TCPListener Instance
        {
            get { return _listener; }
        }

        /// <summary>
        /// Uruchamia nasłuchiwanie na obranym porcie TCP na serwerze.
        /// </summary>
        /// <param name="backlog">Maksymalna ilość połączeń oczekujących</param>
        public void Start(int backlog)
        {
            _tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _tcpSocket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), _port)); //przypisanie do gniazda ip 127.0.0.1 oraz portu
            
            try
            {
                _tcpSocket.Listen(backlog);
                _active = true;

                while (_active)
                {
                    _allDone.Reset();

                    Console.WriteLine("Oczekiwanie na połączenie TCP...");
                    _tcpSocket.BeginAccept(new AsyncCallback(AcceptConnection), _tcpSocket);

                    _allDone.WaitOne();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        public void AcceptConnection(IAsyncResult asyncResult)
        {
            try
            {
                _allDone.Set();
                Console.WriteLine($"Połączono!");

                // Get the socket that handles the client request.  
                Socket listener = (Socket) asyncResult.AsyncState;
                Socket socket = listener.EndAccept(asyncResult);

                // Create the state object.  
                var localAddress = ((IPEndPoint) socket.LocalEndPoint).Address.ToString();
                var localPort = ((IPEndPoint) socket.LocalEndPoint).Port.ToString();

                var destinationAddress = ((IPEndPoint) socket.RemoteEndPoint).Address.ToString();
                var destinationPort = ((IPEndPoint) socket.RemoteEndPoint).Port.ToString();

                var tcpMedium = new TCPMedium(socket, localPort);
                var newConnection = new Connection(destinationAddress, destinationPort, tcpMedium);
                if (_active)
                    tcpMedium.BeginReceive();

                var sessionManager = SessionManager.Instance;
                var session = sessionManager.CreateNewSession(newConnection);

                //wysłanie wiadomości inicjującej do klienta zawierającej id sesji
                var initialMessage = new InitialConnectionMessage(session.ID);
                initialMessage.SetChecksum();
                newConnection.SendMessage(initialMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void Stop()
        {
            try
            {
                _active = false;
                _allDone.Reset();
                _tcpSocket.Close();
            }
            catch
            {
                Console.WriteLine("Problem przy wyłączaniu nasłuchiwania TCP!");
            }
        }

        public bool IsActive()
        {
            return _active;
        }
    }
}