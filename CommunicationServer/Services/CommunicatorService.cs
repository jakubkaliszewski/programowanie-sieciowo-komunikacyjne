using System;
using System.Globalization;
using System.Text;
using CommunicationServer.Database;
using SharedCommunicationLibrary;
using SharedCommunicationLibrary.Message;
using SharedCommunicationLibrary.Services;
using SharedCommunicationLibrary.Session;

namespace CommunicationServer.Services
{
    /*Schemat treści wiadomości dla komunikatora
     - Separator: '|'
     - login adresata,
     - treść właściwa,
     - login nadawcy
     - czas otrzymania
     */

    public sealed class CommunicatorService : IService
    {
        private static readonly CommunicatorService _communicatorServiceInstance = new CommunicatorService();
        private static readonly IMessageRepository MessageRepository;
        private readonly char _separator = '|';
        private bool _active = true;

        static CommunicatorService()
        {
            MessageRepository = DependencyInjection.Container.GetInstance<IMessageRepository>();
        }
        public static CommunicatorService Instance
        {
            get { return _communicatorServiceInstance; }
        }

        /// <summary>
        /// Metoda zlecająca wykonanie akcji adekwatnie do komunikatu w wiadomości
        /// </summary>
        /// <param name="message">Odebrana wiadomość.</param>
        public void DoAction(Message message)
        {
            string content = message.GetContent();
            if (content.StartsWith("show"))
                ShowMessages(message);
            else
                SaveMessage(message);
        }

        public void Start()
        {
            _active = true;
        }

        public void Stop()
        {
            _active = false;
        }

        public bool IsActive()
        {
            return _active;
        }

        /// <summary>
        /// Akcja zapisania otrzymanej - nowej wiadomości na serwerze.
        /// </summary>
        /// <param name="message">Nowa wiadomość</param>
        private void SaveMessage(Message message)
        {
            MessageDb messageToSave = new MessageDb();

            messageToSave.Login = GetRecipientLoginFromMessage(message.GetContent());
            messageToSave.Content = GetContentFromMessage(message.GetContent());
            messageToSave.SenderLogin = GetSenderLoginFromMessage(message.GetContent());
            messageToSave.DateTime = DateTime.UtcNow;
            var date = messageToSave.DateTime;
            MessageRepository.AddMessage(messageToSave);
        }

        /// <summary>
        /// Odpowiada za wywołanie żądania "show" pokaż wiadomości dla mnie.
        /// </summary>
        /// <param name="message">Wiadomość posiadająca dane żądanie.</param>
        private void ShowMessages(Message message)
        {
            var sessionID = message.GetSessionID();

            string login = GetSenderLoginFromMessage(message.GetContent());
            var messagesByLogin = MessageRepository.GetMessagesByLogin(login);

            foreach (var messageDb in messagesByLogin)
            {
                var preparedMessage = PrepareMessageToShow(messageDb, sessionID);
                SendResponse(sessionID, preparedMessage);
            }
        }

        /// <summary>
        /// Dostarcza z wiadomości komunikatora login adresata.
        /// </summary>
        /// <param name="message">Wiadomość dostarczona na serwer.</param>
        /// <returns>Login adresata wiadomości.</returns>
        private string GetRecipientLoginFromMessage(string message)
        {
            return SplitContentMessage(message)[0];
        }

        /// <summary>
        /// Dostarcza z wiadomości komunikatora login nadawcy.
        /// </summary>
        /// <param name="message">Wiadomość dostarczona na serwer.</param>
        /// <returns>Login nadawcy wiadomości.</returns>
        private string GetSenderLoginFromMessage(string message)
        {
            if(SplitContentMessage(message).Length == 4)
                return SplitContentMessage(message)[2];
            return SplitContentMessage(message)[1];
        }

        /// <summary>
        /// Dostarcza z wiadomości komunikatora przekazywaną treść.
        /// </summary>
        /// <param name="message">Wiadomość dostarczona na serwer.</param>
        /// <returns>Treść wiadomości.</returns>
        private string GetContentFromMessage(string message)
        {
            return SplitContentMessage(message)[1];
        }

        /// <summary>
        /// Rozdziela zawartość wiadomości wydobywając z niej parametry konwersacji.
        /// </summary>
        /// <param name="message">Wiadomość dostarczona na serwer.</param>
        /// <returns>Podzielona zawartość wiadomości.</returns>
        private string[] SplitContentMessage(string message)
        {
            var splited = message.Split(_separator);
            if (splited.Length != 3 && splited.Length != 4)
                throw new Exception("Wiadomość komunikatora nie zawiera wszystkich pól!!");
            return splited;
        }

        /// <summary>
        /// Metoda przygotowująca zapisaną wiadomość do wysłania w formacie Message.
        /// </summary>
        /// <param name="messageByLogin">Zapisana wiadomość w bazie.</param>
        /// <param name="sessionID">ID sesji w której odbędzie się wysłanie wiadomości.</param>
        /// <returns>Przygotowana wiadomość do wysłania</returns>
        private Message PrepareMessageToShow(MessageDb messageByLogin, string sessionID)
        {
            Message messageToShow = new Message(ServicesEnum.COMMUNICATOR.ToString("F"), sessionID);

            StringBuilder messageContent = new StringBuilder();
            messageContent.Append(messageByLogin.Login).Append(_separator);
            messageContent.Append(messageByLogin.Content).Append(_separator);
            messageContent.Append(messageByLogin.SenderLogin).Append(_separator);
            messageContent.Append(messageByLogin.DateTime.ToString("G", CultureInfo.CreateSpecificCulture("pl-PL"))).Append(_separator);
            messageToShow.SetContent(messageContent.ToString());

            messageToShow.SetChecksum();
            return messageToShow;
        }

        /// <summary>
        /// Otrzymanie instancji sesji na podstawie jej ID.
        /// </summary>
        /// <param name="sessionID">ID sesji.</param>
        /// <returns>Instancja sesji, która posiada podane ID.</returns>
        private Session GetSessionByID(string sessionID)
        {
            var sessionManager = SessionManager.Instance;
            return sessionManager.GetSessionByID(sessionID);
        }

        /// <summary>
        /// Zlecenie wysłania wiadomości - odpowiedzi w ramach podanej sesji.
        /// </summary>
        /// <param name="sessionID">Identyfikator sesji</param>
        /// <param name="messageToResponse">Wysyłana wiadomość</param>
        private void SendResponse(string sessionID, Message messageToResponse)
        {
            var sessionManager = SessionManager.Instance;
            sessionManager.SendMessage(sessionID, messageToResponse);
        }
    }
}