using SharedCommunicationLibrary;
using SharedCommunicationLibrary.Message;
using SharedCommunicationLibrary.Services;
using SharedCommunicationLibrary.Session;

namespace CommunicationServer.Services
{
    public sealed class PingService : IService
    {
        private static readonly PingService _pingServiceInstance = new PingService();
        private bool _active = true;

        static PingService(){}
        public static PingService Instance
        {
            get { return _pingServiceInstance; }
        }

        public void DoAction(Message message)
        {
            SendResponse(message.GetSessionID(), new Message(ServicesEnum.PING.ToString("F"),message.GetSessionID()));
        }

        public void Start()
        {
            _active = true;
        }

        public void Stop()
        {
            _active = false;
        }

        public bool IsActive()
        {
            return _active;
        }

        /// <summary>
        /// Otrzymanie instancji sesji na podstawie jej ID.
        /// </summary>
        /// <param name="sessionID">ID sesji.</param>
        /// <returns>Instancja sesji, która posiada podane ID.</returns>
        private Session GetSessionByID(string sessionID)
        {
            var sessionManager = SessionManager.Instance;
            return sessionManager.GetSessionByID(sessionID);
        }

        /// <summary>
        /// Zlecenie wysłania wiadomości - odpowiedzi w ramach podanej sesji.
        /// </summary>
        /// <param name="sessionID">Identyfikator sesji</param>
        /// <param name="messageToResponse">Wysyłana wiadomość</param>
        private void SendResponse(string sessionID, Message messageToResponse)
        {
            var sessionManager = SessionManager.Instance;
            sessionManager.SendMessage(sessionID, messageToResponse);
        }
    }
}