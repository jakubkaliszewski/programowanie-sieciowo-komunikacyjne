using System;
using System.ComponentModel;
using System.Text;
using Tsunami_Server.Listeners;
using TsunamiShared;
using TsunamiShared.Medium;
using TsunamiShared.Services;
using TsunamiShared.Session;

namespace Tsunami_Server.Services
{
    /// <summary>
    /// Serwis CONFIG po stronie serwera.
    /// </summary>
    public sealed class ConfigService : IService
    {
        private static readonly ConfigService _configServiceInstance = new ConfigService();
        private bool _active = true;

        static ConfigService()
        {
            SessionManager.Instance.ListenerToConfig += StartOrStopListener;
        }

        public static ConfigService Instance
        {
            get { return _configServiceInstance; }
        }

        private static void StartOrStopListener(bool active, MediumType mediumType)
        {
            switch (mediumType)
            {
                case MediumType.TCP:
                {
                    var listener = TCPListener.Instance;
                    if (active && !listener.IsActive())
                        listener.Start(5);
                    else if(listener.IsActive())
                        listener.Stop();

                    break;
                }
                case MediumType.UDP:
                {
                    var listener = UDPListener.Instance;
                    if (active && !listener.IsActive())
                        listener.Start();
                    else if(listener.IsActive())
                        listener.Stop();

                    break;
                }
            }
        }

        /// <summary>
        /// Metoda zlecająca wykonanie akcji adekwatnie do komunikatu w wiadomości
        /// </summary>
        /// <param name="message">Odebrana wiadomość.</param>
        public void DoAction(Message message)
        {
            string content = message.GetContent();
            if (content.Equals("show"))
            {
                Message messageToSend = new Message(ServicesEnum.CONFIG.ToString("F"), message.GetSessionID());
                PrepareShowMessage(messageToSend);
                SendResponse(messageToSend.GetSessionID(), messageToSend);
            }
            else
            {
                DoManagementAction(content);
            }
        }

        public void Start()
        {
            _active = true;
        }

        public void Stop()
        {
            _active = false;
        }

        public bool IsActive()
        {
            return _active;
        }

        /// <summary>
        /// Wykonuje akcję zarządzającą medium bądź serwisem.
        /// </summary>
        /// <param name="content"></param>
        private void DoManagementAction(string content)
        {
            var split = content.Split('|');
            try
            {
                if (split.Length != 2)
                    throw new Exception("Wiadomość posiada za mało wymaganych argumentów by konfigurować serwer!!");

                var activeFlag = split[1].Equals("True");

                string serviceOrMedium = split[0].ToUpper();

                if (serviceOrMedium.Equals(MediumType.TCP.ToString("F")))
                {
                    var listener = TCPListener.Instance;
                    if (activeFlag && !listener.IsActive())
                        listener.Start(5);
                    else if(listener.IsActive())
                        TCPListener.Instance.Stop();
                }

                if (serviceOrMedium.Equals(MediumType.UDP.ToString("F")))
                {
                    var listener = UDPListener.Instance;
                    if (activeFlag && !listener.IsActive())
                        listener.Start(5);
                    else if(listener.IsActive())
                        TCPListener.Instance.Stop();
                }

                if (serviceOrMedium.Equals(ServicesEnum.CONFIG.ToString("F")))
                {
                    if (activeFlag)
                        this.Start();
                    else
                        this.Stop();
                }

                if (serviceOrMedium.Equals(ServicesEnum.FTP_SERVICE.ToString("F")))
                {
                    if (activeFlag)
                        FTPService.Instance.Start();
                    else
                        FTPService.Instance.Stop();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void PrepareShowMessage(Message message)
        {
            //sprawdzam statusy listenerów             
            string TCPStatus = TCPListener.Instance.IsActive().ToString();
            string UDPStatus = UDPListener.Instance.IsActive().ToString();

            string ConfigStatus = this.IsActive().ToString();
            string FTPStatus = FTPService.Instance.IsActive().ToString();

            StringBuilder builder = new StringBuilder();
            builder.Append($"{MediumType.TCP:F}|{TCPStatus}\"");
            builder.Append($"{MediumType.UDP:F}|{UDPStatus}\"");


            builder.Append($"{ServicesEnum.CONFIG:F}|{ConfigStatus}\"");
            builder.Append($"{ServicesEnum.FTP_SERVICE:F}|{FTPStatus}\"");

            message.SetContent(builder.ToString());
            message.SetChecksum();
        }

        private bool IsMediumExist(string mediumName)
        {
            var enumValues = Enum.GetValues(typeof(MediumType));
            foreach (MediumType medium in (MediumType[]) enumValues)
            {
                if (mediumName.Equals(Description(medium)))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Metoda sprawdzająca czy serwis o podanej nazwie istnieje.
        /// </summary>
        /// <param name="serviceName">Nazwa serwisu.</param>
        /// <returns>Rezultat</returns>
        private bool IsServiceExist(string serviceName)
        {
            var enumValues = Enum.GetValues(typeof(ServicesEnum));
            foreach (ServicesEnum service in (ServicesEnum[]) enumValues)
            {
                if (serviceName.Equals(Description(service)))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Otrzymanie instancji sesji na podstawie jej ID.
        /// </summary>
        /// <param name="sessionID">ID sesji.</param>
        /// <returns>Instancja sesji, która posiada podane ID.</returns>
        private Session GetSessionByID(string sessionID)
        {
            var sessionManager = SessionManager.Instance;
            return sessionManager.GetSessionByID(sessionID);
        }

        /// <summary>
        /// Zlecenie wysłania wiadomości - odpowiedzi w ramach podanej sesji.
        /// </summary>
        /// <param name="sessionID">Identyfikator sesji</param>
        /// <param name="messageToResponse">Wysyłana wiadomość</param>
        private void SendResponse(string sessionID, Message messageToResponse)
        {
            var sessionManager = SessionManager.Instance;
            sessionManager.SendMessage(sessionID, messageToResponse);
        }

        private static string GetCustomDescription(object objEnum)
        {
            var field = objEnum.GetType().GetField(objEnum.ToString());
            var attributes = (DescriptionAttribute[]) field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description.ToLower() : objEnum.ToString().ToLower();
        }

        private static string Description(Enum value)
        {
            return GetCustomDescription(value);
        }
    }
}