using System;
using Tsunami_Server.Services;
using TsunamiShared;
using TsunamiShared.Services;

namespace Tsunami_Server
{
    public sealed class ServisesManager : IServisesManager
    {
        private static readonly ServisesManager _servisesManagerInstance = new ServisesManager();
        static ServisesManager(){}
        public static ServisesManager Instance
        {
            get { return _servisesManagerInstance; }
        }
        
        /// <summary>
        /// Przekazuje otrzymaną wiadomość do odpowiedniego serwisu na podstawie zadeklarowanej nazwy serwisu w wiadomości
        /// </summary>
        /// <param name="message">Przekazywana wiadomość</param>
        /// <exception cref="ArgumentException">Wyjątek rzucany w przypadku podania przez wiadomość błędnej nazwy serwisu.</exception>
        public void DelegateMessageToService(Message message)
        {
            var serviceNameFromMessage = message.GetServiceName().ToUpper();
            Enum.TryParse(serviceNameFromMessage, out ServicesEnum serviceFromMessage);
            
            switch (serviceFromMessage)
            {
                case ServicesEnum.FTP_SERVICE:
                {
                    var ftp = FTPService.Instance;
                    if (ftp.IsActive())
                    {
                        ftp.DoAction(message);
                    }
                    break;
                } 
                case ServicesEnum.CONFIG:
                {
                    var config = ConfigService.Instance;
                    if(config.IsActive())
                        config.DoAction(message);

                    break;
                }
                default:
                    throw new ArgumentException("Odczytany z wiadomości typ serwisu nie istnieje!!");
            }
        }
    }
}