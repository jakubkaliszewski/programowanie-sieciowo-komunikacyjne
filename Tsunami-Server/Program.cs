﻿using System.Threading.Tasks;
using SimpleInjector;
using Tsunami_Server.Listeners;
using TsunamiShared;
using TsunamiShared.Services;
using DbContext = Tsunami_Server.Database.DbContext;
using FileRepository = Tsunami_Server.Database.FileRepository;
using IDbContext = Tsunami_Server.Database.IDbContext;
using IFileRepository = Tsunami_Server.Database.IFileRepository;

namespace Tsunami_Server
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            SetDependencyInjectionConfig();
            TCPListener tcpListener = new TCPListener();
            new Task(() => tcpListener.Start(5)).Start();

            while (true)
            {
            }
        }

        /// <summary>
        /// Zawiera ustawienia dot wstrzykiwania zależności dla serwera.
        /// Tu także rejestruje się nowe rozwiązania w kontenerze.
        /// </summary>
        private static void SetDependencyInjectionConfig()
        {
            var container = new Container();
            // 2. Configure the container (register)
            container.Register<IServisesManager, ServisesManager>();
            container.Register<IDbContext, DbContext>();
            container.Register<IFileRepository, FileRepository>();
            // 3. Verify your configuration
            container.Verify();
            DependencyInjection.SetRegisteredContainer(container);
        }
    }
}