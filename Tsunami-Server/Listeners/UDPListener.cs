﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TsunamiShared;
using TsunamiShared.Connections;
using TsunamiShared.Medium;
using TsunamiShared.Session;

namespace Tsunami_Server.Listeners
{
    public sealed class UDPListener : IListener
    {
        private static UDPListener _listener = new UDPListener();
        private static Socket _udpSocket;
        private static int _port;
        private static bool _active;
        private byte[] _byteData = new byte[2048];

        static UDPListener()
        {
            //ważne by uaktywnić dane urządzenie by było w stanie odebrać żądanie
            _port = Int32.Parse(ConfigurationManager.AppSettings["UDPPort"]);
        }
        public static UDPListener Instance
        {
            get { return _listener; }
        }
        /// <summary>
        /// Uruchamia nasłuchiwanie na obranym porcie UDP na serwerze.
        /// </summary>
        /// <param name="backlog">Maksymalna ilość połączeń oczekujących</param>
        public void Start(int backlog = 1)
        {
            _udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _udpSocket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), _port)); //przypisanie do gniazda ip 127.0.0.1 oraz portu
            try
            {
                _active = true;
                Console.WriteLine("Oczekiwanie na połączenie UDP...");
                
                while (_active)
                {
                    if (_udpSocket.Receive(_byteData) > 0)
                    {
                        byte[] dataToMethod = new byte[_byteData.Length];
                        _byteData.CopyTo(dataToMethod, 0);
                        
                        new Task(()=>AcceptConnection(dataToMethod)).Start();
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
        private void AcceptConnection(byte[] buffer)
        {
            try
            {
                //klient wysłał swoją wiadomość przedstawiającą
                Console.WriteLine($"Połączono!");
                
                int size = buffer.Length;
                string data = Encoding.ASCII.GetString(buffer);
                if (data.Length != size)
                {
                    Console.WriteLine("Otrzymano niepełne dane od Klienta!");
                    return;
                }
                
                var messagesInBuffer = data.Split('\n').ToList();

                var splited = messagesInBuffer[0].Split(';');
                if (splited.Length != 6)
                    throw new Exception("Otrzymano niepełne dane od Klienta!");

                string messageID = splited[0];
                string sessionID = splited[1];
                string serviceName = splited[2];
                string content = splited[3];
                string checkSum = splited[4];

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(messageID);
                stringBuilder.Append(sessionID);
                stringBuilder.Append(serviceName);
                stringBuilder.Append(content);

                if (!CryptographyModule.VerifyMd5Hash(stringBuilder.ToString(), checkSum))
                    throw new CryptographicException(
                        "Suma kontrolna nie zgadza się z zadeklarowaną sumą kontrolną otrzymaną w wiadomości!!");

                //wyciągam z niej jego adres IP
                var splitedContent = content.Split('|');
                string remoteIP = splitedContent[0];
                string remotePort = splitedContent[1];
                var remoteEP = new IPEndPoint(IPAddress.Parse(remoteIP), Int32.Parse(remotePort));
                
                //po stronie serwera tworzę nowe gniazdo, 
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                socket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 0));
                
                //pobierz otrzymany numer portu
                var socketLocalEndPoint = (IPEndPoint) (socket.LocalEndPoint);
                var localPort = socketLocalEndPoint.Port.ToString();
                
                //wysyłam klientowi port w celu by na nie wysyłał dane
                var initialUDPMessage = new ConnectionLessInitialConnectionMessage(
                    "",
                    localPort
                );
                _udpSocket.SendTo(
                    new BufferService().PrepareMessageToSend(initialUDPMessage), 
                    SocketFlags.None,
                    remoteEP
                );
                
                var udpMedium = new UDPMedium(socket, remotePort, remoteEP);
                var newConnection = new Connection(remoteIP, remotePort, udpMedium);

                var sessionManager = SessionManager.Instance;
                var session = sessionManager.CreateNewSession(newConnection);
                Thread.Sleep(10000);
                //wysłanie wiadomości inicjującej do klienta zawierającej id sesji
                var initialMessage = new InitialConnectionMessage(session.ID);
                initialMessage.SetChecksum();
                newConnection.SendMessage(initialMessage);
                Console.WriteLine("Wysłano wiadomość z sesją do klienta!");
                
                if (_active)
                    udpMedium.BeginReceive();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        public void Stop()
        {
            try
            {
                _active = false;
                _udpSocket.Close();
            }
            catch
            {
                Console.WriteLine("Problem przy wyłączaniu nasłuchiwania UDP!");
            }
        }

        public bool IsActive()
        {
            return _active;
        }
    }
}
