namespace Tsunami_Server.Listeners
{
    public interface IListener
    {
        void Start(int backlog);
        void Stop();
        bool IsActive();
    }
}