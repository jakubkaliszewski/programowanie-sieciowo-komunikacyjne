using LiteDB;

namespace Tsunami_Server.Database
{
    public interface IDbContext
    {
        LiteDatabase GetDb();
    }
}