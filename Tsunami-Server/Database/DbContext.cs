using System.Configuration;
using LiteDB;

namespace Tsunami_Server.Database
{
    public class DbContext : IDbContext
    {
        private string _connectionString;
        private LiteDatabase _db;
        
        public DbContext()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["LiteDB"].ConnectionString;
            _db = new LiteDatabase(_connectionString);
        }

        public LiteDatabase GetDb()
        {
            return _db;
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}