namespace Tsunami_Server.Database
{
    public interface IFileRepository
    {
        void AddFile(File file);
        void DeleteFile(File file);
        File GetFileByAlias(string alias);
        bool IsFileExist(string alias);
    }
}