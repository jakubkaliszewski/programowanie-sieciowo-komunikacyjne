using System;
using SimpleInjector;

namespace TsunamiShared
{
    /// <summary>
    /// Klasa przechowująca skonfigurowany kontener używany do wstrzykiwania zależności.
    /// </summary>
    public static class DependencyInjection
    {
        private static Container _container;
        public static void SetRegisteredContainer(Container container)
        {
            _container = container ?? throw new Exception("Wprowadzony kontener SimpleIjector jest pusty!!");
        }

        public static Container Container
        {
            get => _container;
        }
    }
}