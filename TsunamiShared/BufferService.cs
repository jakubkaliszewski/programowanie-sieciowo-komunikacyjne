using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TsunamiShared.Medium;
using TsunamiShared.Services;

namespace TsunamiShared
{
    public sealed class BufferService
    {
        private readonly IServisesManager _servisesManager;
        private readonly char _separator = ';';
        public BufferService()
        {
            _servisesManager = DependencyInjection.Container.GetInstance<IServisesManager>();
        }

        public void Read(byte[] buffer, AbstractMedium medium)
        {
            int size = buffer.Length;
            string data = Encoding.ASCII.GetString(buffer);
            if (data.Length != size)
            {
                Console.WriteLine("Otrzymano niepełne dane od Klienta!");
                //TODO poproś o ponowienie
                // Not all data received. Get more.  
                medium.BeginReceive();
                //handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,  przeciążyć z abstrackmedium beginreceive by podawać bufor oraz jego wielkość
                //    new AsyncCallback(ReadCallback), state);  
                return;
            }

            var receivedMessage = CreateMessage(data);
            medium.BeginReceive();

            try
            {
                foreach (var message in receivedMessage)
                {
                    new Task(() => _servisesManager.DelegateMessageToService(message)).Start();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
 
            //czyta, tworzy wiadomość
            //Przekazywane do menegera serwisów
        }

        private IList<Message> CreateMessage(string dataFromBuffer)
        {
            /*
             * Schemat wiadomości:
             * - id wiadomości
             * - id sesji
             * - nazwa usługi
             * - treść
             * - suma kontrolna
             * Separator: ; (średnik)
             */
            IList<Message> receivedMessages = new List<Message>();
            var messagesInBuffer = dataFromBuffer.Split('\n').ToList();
            for (int i = 0; i < messagesInBuffer.Count -1; i++)
            {
                var splited = messagesInBuffer[i].Split(_separator);
                if (splited.Length != 6)
                    throw new Exception("Otrzymano niepełne dane od Klienta!");

                string messageID = splited[0];
                string sessionID = splited[1];
                string serviceName = splited[2];
                string content = splited[3];
                string checkSum = splited[4];

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(messageID);
                stringBuilder.Append(sessionID);
                stringBuilder.Append(serviceName);
                stringBuilder.Append(content);

                if (!CryptographyModule.VerifyMd5Hash(stringBuilder.ToString(), checkSum))
                    throw new CryptographicException(
                        "Suma kontrolna nie zgadza się z zadeklarowaną sumą kontrolną otrzymaną w wiadomości!!");

                Message receivedMessage = new Message(messageID, serviceName, sessionID);
                receivedMessage.SetContent(content);
                
                receivedMessages.Add(receivedMessage);
            }

            return receivedMessages;
        }

        /// <summary>
        /// Przygotowuje wiadomość do wysłania zamieniając ją w ciąg byte[]
        /// </summary>
        /// <param name="message">Wiadomość, która bd przesłana.</param>
        /// <returns>Gotowa do przesłania przez medium wiadomość.</returns>
        public byte[] PrepareMessageToSend(Message message)
        {
            if (message.IsEditable)
                message.SetChecksum();

            string messageString = message.ToString();
            var bytes = Encoding.ASCII.GetBytes(messageString);

            return bytes;
        }
    }
}