using System;

namespace TsunamiShared
{
    public class InitialConnectionMessage : Message
    {
        static string serviceName = "init";

        public InitialConnectionMessage(string sessionId) : base(serviceName, sessionId)
        {
            _sessionID = sessionId ?? throw new Exception("ID sesji jest pusty!!");
            _id = Guid.NewGuid().ToString();
        }
    }
}