using System.ComponentModel;

namespace TsunamiShared.Services
{
    /// <summary>
    /// Enum zawierający czytelną listę dostępnych serwisów.
    /// Enum wykorzystywany zarówno w aplikacji klienckiej i w serwerowej.
    /// </summary>
    public enum ServicesEnum
    {
        /// <summary>
        /// Pobieranie plików z serwera, składowanie plików na serwerze.
        /// </summary>
        [Description("FTP")]
        FTP_SERVICE,
        /// <summary>
        /// Konfiguracja serwera (uruchamianie i wstrzymywanie usług, mediów komunikacyjnych, odpytywanie o dostępne usługi i media).
        /// </summary>
        [Description("CFG")]
        CONFIG
    }
}