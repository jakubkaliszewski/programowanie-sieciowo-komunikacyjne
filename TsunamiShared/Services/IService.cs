namespace TsunamiShared.Services
{
    /// <summary>
    /// Interfejs dla wszelkich serwisów w systemie
    /// </summary>
    public interface IService
    {
        void DoAction(Message message);
        void Start();
        void Stop();
        bool IsActive();
    }
}