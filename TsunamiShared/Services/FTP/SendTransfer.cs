using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Timers;

namespace TsunamiShared.Services.FTP
{
    public class SendTransfer : ActiveTransfer
    {
        private const int _bytesInFilePackage = 1024;
        private byte[] _buffer;
        private int _packageCounter;
        private string _fileName;
        public delegate void SendAgainUnconfirmedPackages(SendTransfer transfer); 
        private static Timer _unconfirmedPackagesTimer;

        public event SendAgainUnconfirmedPackages OnSendAgainUnconfirmedPackages;
        public string FileName => _fileName;

        public SendTransfer(Guid id, string fileName, string sessionId, string resourceName) : base(id, sessionId, resourceName)
        {
            _stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            _buffer = new byte[_bytesInFilePackage + 10];
            _fileName = fileName;
        }

        public SendTransfer(string fileName, string sessionId, string resourceName) : base(sessionId, resourceName)
        {
            _stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            _buffer = new byte[_bytesInFilePackage + 10];
            _fileName = fileName;
        }
        
        private void InitTimer()
        {
            _unconfirmedPackagesTimer = new System.Timers.Timer();
            _unconfirmedPackagesTimer.Interval = 3000;
            _unconfirmedPackagesTimer.Elapsed += OnCheckUnconfirmedPackages;
            _unconfirmedPackagesTimer.AutoReset = true;

            _unconfirmedPackagesTimer.Enabled = true;
        }

        public IEnumerable<FilePackage> GetNextFilePackage()
        {
            int bytesToRead = (int)_stream.Length;
            int bytesRead = 0;

            do
            {
                // mogę otrzymać od 0 do 1024.
                bytesRead = _stream.Read(_buffer, 0, _bytesInFilePackage);
                bytesToRead -= bytesRead;
                
                string data = Encoding.ASCII.GetString(_buffer);
                yield return new FilePackage(_packageCounter, data);
                _packageCounter++;
            } while (bytesToRead > 0);
        }

        protected void OnCheckUnconfirmedPackages(object source, ElapsedEventArgs e)
        {
            if (_unconfirmedPackages.Count == 0 && _endFlag)
            {
                CloseStream();
                _unconfirmedPackagesTimer.Elapsed -= OnCheckUnconfirmedPackages;
                _unconfirmedPackagesTimer.Close();
                Console.WriteLine("Zakończono transfer pliku!!");
            }
            else
            {
                //resend pakietów z listy
                OnSendAgainUnconfirmedPackages(this);
            }
        }
    }
}