using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using TsunamiShared.Medium;

namespace TsunamiShared.Session
{
    public sealed class SessionManager //ma być singletonem
    {
        private static SessionManager _sessionManagerInstance = new SessionManager();
        private static ICollection<Session> _activeSessions;
        private int _TCPSessionsCounter;
        private int _UDPSessionsCounter;
        private static int _connectionLimit;

        public delegate void ListenerToConfigEventHandler(bool active, MediumType mediumType);
        public event ListenerToConfigEventHandler ListenerToConfig;
        static SessionManager()
        {
            _activeSessions = new LinkedList<Session>();
            _connectionLimit = Int32.Parse(ConfigurationManager.AppSettings["ConnectionLimit"]);
        }

        public static SessionManager Instance
        {
            get { return _sessionManagerInstance; }
        }


        /// <summary>
        /// Utworzenie nowej sesji dla połączeń 
        /// </summary>
        /// <param name="socket"></param>
        public Session CreateNewSession(Connections.Connection connection)
        {
            if (CanAddNewSession(connection.Medium.GetMediumType()))
            {
                var newSession = new Session(connection);
                _activeSessions.Add(newSession);
                Console.WriteLine("Dodano nową sesję połączenia!");
                return newSession;
            }
            else
            {
                throw new Exception(
                    $"Przekroczono limit dostępnych sesji dla połączenia typu {connection.Medium.GetMediumType()}");
            }
        }

        /// <summary>
        /// Dodanie nowej sesji, na podstawie otrzymanego id sesji 
        /// </summary>
        public void AddSession(Session session)
        {
            _activeSessions.Add(session);
            Console.WriteLine("Dodano nową sesję połączenia!");
        }
        
        private bool CanAddNewSession(MediumType type)
        {
            try
            {
                switch (type)
                {
                    case MediumType.TCP:
                    {
                        if (_TCPSessionsCounter < _connectionLimit)
                        {
                            _TCPSessionsCounter++;
                            if (_TCPSessionsCounter == _connectionLimit)
                                ListenerToConfig(false, MediumType.UDP);
                            return true;
                        }

                        break;
                    }
                    case MediumType.UDP:
                    {
                        if (_UDPSessionsCounter < _connectionLimit)
                        {
                            _UDPSessionsCounter++;
                            if (_UDPSessionsCounter == _connectionLimit)
                                ListenerToConfig(false, MediumType.UDP);
                            return true;
                        }

                        break;
                    }

                    default:
                        throw new ArgumentException();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return false;
        }

        /// <summary>
        /// Odpowiada funkcjonalnością na komunikat exit.
        /// </summary>
        /// <param name="sessionToClose">Sesja, która ma zostać zamknięta.</param>
        public void CloseSession(Session sessionToClose)
        {
            try
            {
                var medium = sessionToClose.Connection.Medium.GetMediumType();
                switch (medium)
                {
                    case MediumType.TCP:
                    {
                        if (_TCPSessionsCounter == _connectionLimit)
                            ListenerToConfig(true, MediumType.TCP);

                        _TCPSessionsCounter--;
                        _activeSessions.Remove(sessionToClose);
                        break;
                    }
                    case MediumType.UDP:
                    {
                        if (_UDPSessionsCounter == _connectionLimit)
                            ListenerToConfig(true, MediumType.UDP);

                        _UDPSessionsCounter--;
                        _activeSessions.Remove(sessionToClose);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Zwraca sesję odpowiadającą podanemu id sesji.
        /// </summary>
        /// <param name="sessionID">Id szukanej sesji.</param>
        /// <returns>Sesja posiadająca podane ID.</returns>
        /// <exception cref="Exception">Wyjątek rzucany w przypadku braku sesji o podanym identyfiktorze.</exception>
        public Session GetSessionByID(string sessionID)
        {
            var result = _activeSessions.FirstOrDefault(session => session.ID.Equals(sessionID));
            if (result != null)
                return result;
            throw new Exception($"Nie znaleziono sesji o id: {sessionID}!!");
        }

        /// <summary>
        /// Sprawdza czy wprowadzony identyfikator odpowiada jakiejkolwiek aktywnej sesji.
        /// </summary>
        /// <param name="sessionID">Wprowadzony identyfikator sesji.</param>
        /// <returns>Rezultat czy sesja o podanym identyfikatorze istnieje.</returns>
        public bool IsSessionExist(string sessionID)
        {
            var result = _activeSessions.FirstOrDefault(session => session.ID.Equals(sessionID));
            if (result != null)
                return true;
            return false;
        }

        /// <summary>
        /// Wysyła przygotowaną w serwisie wiadomość w ramach podanej sesji.
        /// </summary>
        /// <param name="sessionId">Identyfikator sesji, która ma zostać wykorzystana.</param>
        /// <param name="messageToResponse">Przygotowana wiadomość przez serwis do wysłania.</param>
        public void SendMessage(string sessionId, Message messageToResponse)
        {
            var session = GetSessionByID(sessionId);
            session.Connection.SendMessage(messageToResponse);
        }

        public int GetSessionCount()
        {
            return _activeSessions.Count;
        }
    }
}