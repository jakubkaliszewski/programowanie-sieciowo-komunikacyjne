using System;
using TsunamiShared.Medium;

namespace TsunamiShared.Connections
{
    public class ConnectionFactory
    {
        public Connection InitializeConnection(MediumType mediumType, string port, string address = null)
        {
            //ustawiamy port lokalnie, w zależności od rodzaju medium
            var medium = GetMedium(mediumType, port);
            
            if (medium.ConnectTo(port, address))
                return new Connection(port, address, medium);
            throw new Exception($"Nie udało się utworzyć połączenia dla {address}:{port}");
        }

        protected AbstractMedium GetMedium(MediumType mediumType)
        {
            MediumFactory factory = new MediumFactory();
            return factory.CreateMedium(mediumType);
        }
        
        protected AbstractMedium GetMedium(MediumType mediumType, string port)
        {
            MediumFactory factory = new MediumFactory();
            return factory.CreateMedium(mediumType, port);
        }
    }
}