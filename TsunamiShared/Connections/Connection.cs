using TsunamiShared.Medium;

namespace TsunamiShared.Connections
{
    public class Connection
    {
        private string _destinationPort;
        private string _destinationAddress;
        private AbstractMedium _medium;//może mówić o wykorzystywanym medium

        public Connection(string port, string address, AbstractMedium medium)
        {
            _destinationPort = port;
            _destinationAddress = address;
            _medium = medium;
        }

        public string DestinationPort => _destinationPort;

        public string DestinationAddress => _destinationAddress;

        public AbstractMedium Medium => _medium;

        public void SendMessage(Message message)
        {
           _medium.SendMessage(message);
        }
    }
}