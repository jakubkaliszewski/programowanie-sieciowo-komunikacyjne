using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace TsunamiShared.Medium
{
    /// <summary>
    /// Medium komunikacjyjne protokołu TCP
    /// </summary>
    public class TCPMedium : AbstractMedium
    {
        private Socket _tcpSocket;
        private string _destinationAddress;
        private string _destinationPort;

        public TCPMedium(string destinationPortName) : base(destinationPortName)
        {
            //ważne by uaktywnić dane urządzenie by było w stanie odebrać żądanie
            _tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _tcpSocket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), Int32.Parse(ConfigurationManager.AppSettings["TCPPort"])));//przypisanie do gniazda ip 127.0.0.1 oraz portu
            _connected = false;
            _mediumType = MediumType.TCP;
        }

        public TCPMedium(Socket tcpSocket, string destinationPortName) : base(destinationPortName)
        {
            _tcpSocket = tcpSocket;
            _connected = tcpSocket.Connected;
            _mediumType = MediumType.TCP;
        }

        public override bool ConnectTo(string destinationPort, string destinationAddress)
        {
            try
            {
                Console.WriteLine($"Nawiązywanie połączenia TCP z {destinationAddress}:{destinationPort}...");
                // Połączenie używając limitu czasu oczekiwania na jego nawiązanie

                int counter = 0;
                while (!_connected && counter < _timeout)
                {
                    _tcpSocket.BeginConnect(destinationAddress, Int32.Parse(destinationPort), null, null);
                    counter+=2000;
                    Thread.Sleep(2000);
                    _connected = _tcpSocket.Connected;
                }
                
                if (_connected)
                {
                    Console.WriteLine($"Nawiązano połączenie z {destinationAddress}:{destinationPort}");
                    _destinationAddress = destinationAddress;
                    _destinationPort = destinationPort;
                }
                else
                {
                    _tcpSocket.Close();
                    throw new TimeoutException($"Przekroczenie czasu połączenia z serwerem {destinationAddress}:{destinationPort}.");
                }
            }
            catch (TimeoutException exception)
            {
                throw new Exception($"Błąd połączenia z serwerem {destinationAddress}:{destinationPort}.", exception);
            }

            return _connected;
        }

        public override bool Disconnect()
        {
            if (_connected)
            {
                _tcpSocket.Disconnect(false);
                _connected = false;
                return true;
            }

            return false;
        }
        
        public override string ToString()
        {
            if (_connected)
                return $"Połączenie TCP z {_destinationAddress}:{_destinationPort}.";
            
            return "Brak połączenia TCP.";
        }
        public override void Dispose()
        { 
            _tcpSocket.Dispose();
        }

        public override void SendMessage(Message message)
        {
            var bytes = BufferService.PrepareMessageToSend(message);
            _tcpSocket.Send(bytes);
        }
        public override void BeginReceive()
        {
            CleanBuffer();
            _tcpSocket.BeginReceive(_buffer, 0, _bufferSize, 0, ReceiveCallback, this);
        }
        protected override void ReceiveCallback(IAsyncResult asyncResult)
        {
            // Retrieve the state object and the handler socket  
            // from the asynchronous state object.  
            TCPMedium state = (TCPMedium) asyncResult.AsyncState;  
            Socket handler = state._tcpSocket;  
  
            // Read data from the client socket.   
            int bytesRead = handler.EndReceive(asyncResult);  
            if(bytesRead > 0)
                BufferService.Read(_buffer, this);
        }
    }
}