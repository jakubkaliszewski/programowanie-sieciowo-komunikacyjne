using System.ComponentModel;

namespace TsunamiShared.Medium
{
    /// <summary>
    /// Enum posiadający wszystkie możliwe typy oferowanych połączeń.
    /// </summary>
    public enum MediumType
    {
        [Description("TCP Connection")]
        TCP,
        [Description("UDP Connection")]
        UDP
    }
    
}