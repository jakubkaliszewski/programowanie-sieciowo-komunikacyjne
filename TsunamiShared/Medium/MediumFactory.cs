using System;
using System.Configuration;

namespace TsunamiShared.Medium
{
    public class MediumFactory
    {
        /// <summary>
        /// Zwraca obiekt odpowiedniego medium transmisyjnego.
        /// </summary>
        /// <param name="type">Enum posiadający wybór użytkownika jakie medium transmisyjne chce użyć</param>
        /// <returns>Zwracana jest instancja wybranego medium transmisyjnego.</returns>
        /// <exception cref="ArgumentException"></exception>
        public AbstractMedium CreateMedium(MediumType type)
        {
            string port;
            switch (type)
            {
                case MediumType.TCP:
                {
                    port = ConfigurationManager.AppSettings["TCPPort"];
                    return new TCPMedium(port);
                }
                case MediumType.UDP:
                {
                    port = ConfigurationManager.AppSettings["UDPPort"];
                    return new UDPMedium(port);
                }

                default:
                    throw new ArgumentException();
            }
        } 
        
        /// <summary>
        /// Zwraca obiekt odpowiedniego medium transmisyjnego.
        /// </summary>
        /// <param name="type">Enum posiadający wybór użytkownika jakie medium transmisyjne chce użyć</param>
        /// <returns>Zwracana jest instancja wybranego medium transmisyjnego.</returns>
        /// <exception cref="ArgumentException"></exception>
        public AbstractMedium CreateMedium(MediumType type, string port)
        {
            switch (type)
            {
                case MediumType.TCP:
                {
                    return new TCPMedium(port);
                }
                case MediumType.UDP:
                {
                    return new UDPMedium(port);
                }

                default:
                    throw new ArgumentException();
            }
        } 
    }
}