using System;
using System.Security.Cryptography;
using System.Text;

namespace TsunamiShared
{
    /// <summary>
    /// Moduł dostarczający obsługę hash-y MD5 na potrzeby wyznaczania sum kontrolnych wiadmomości.
    /// </summary>
    public static class CryptographyModule
    {
        /// <summary>
        /// Zwraca hash MD5 w postaci stringa.
        /// </summary>
        /// <param name="input">Treść, na bazie, której wyznaczony ma być hash.</param>
        /// <returns>Hash stworzony z pomocą MD5.</returns>
        public static string GetMd5Hash(string input)
        {
            MD5 md5Hash = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.ASCII.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        /// <summary>
        /// Sprawdza czy dla podanego stringa hash będzie identyczny do zadeklarowanego.
        /// </summary>
        /// <param name="input">String na podstawie, którego będzie generowany hash.</param>
        /// <param name="hash">Hash do którego będzie porównywany hash utworzony z inputu.</param>
        /// <returns>Wynik czy hashe są identyczne.</returns>
        public static bool VerifyMd5Hash(string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
                return true;

            return false;
        }
    }
}