﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBClient
{
    public static class MBExtensions
    {
        public static void SetTwoBytes(this byte[] tab, int value, int offset)
        {
            tab[offset] = (byte)(value >> 8);
            tab[offset+1] = (byte)value;
        }
        public static ushort GetTwoBytes(this byte[] tab, int offset)
        {
            ushort value = (ushort)((tab[offset] << 8) | tab[offset + 1]);
            return value;
        }
    }
}
