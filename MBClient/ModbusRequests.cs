﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBClient
{
    public class WMRRequest : ModbusMessage
    {
        public WMRRequest(ushort[] data, int offset)
        {
            Length = 6 + 2 * data.Length;
            Function = 0x10;
            Data.SetTwoBytes(offset, 1);
            Data.SetTwoBytes(data.Length, 3);
            Data[5] = (byte)(2 * data.Length);
            for (int i = 0; i < data.Length; i++)
                Data.SetTwoBytes(data[i], 6 + 2 * i);
        }
    }
}
