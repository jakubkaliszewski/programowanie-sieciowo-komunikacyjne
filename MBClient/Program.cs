﻿using System;
using System.Linq;

namespace MBClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wybierz wersję:\n 1 - TCP,\n 2 - Serial");
            var answer = Int32.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
            if(answer == 1)
                FlagaMB(args);
            if (answer == 2)
                FlagaMBSerial();
        }

        static void Flaga(string[] args)
        {
            float[,] p = JasnośćObrazu("POLA0001.GIF", 10, 10);
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                    Console.Write(p[j, i] < 0.6f ? "##" : "..");
                Console.WriteLine();
            }
        }
        public static float[,] JasnośćObrazu(string plik, int szer, int wys)
        {
            float[,] punkty = new float[szer, wys];
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(plik);
            int sz = bmp.Width / szer;
            int wy = bmp.Height / wys;
            for (int i = 0; i < wys; i++)
                for (int j = 0; j < szer; j++)
                {
                    punkty[i, j] = 0;
                    for (int k = 0; k < wy; k++)
                        for (int l = 0; l < sz; l++)
                            punkty[i, j] += bmp.GetPixel(j * sz + l, i * wy + k).GetBrightness();
                    punkty[i, j] /= wy * sz;
                }
            return punkty;
        }

        static void FlagaMB(string[] args)
        {
            ModbusClient client = new ModbusTCPClient("localhost", 502);

            float[,] p = JasnośćObrazu("POLA0001.GIF", 10, 10);
            for (int i = 0; i < 10; i++)
            {
                ushort[] data = Enumerable.Range(0, 10).Select(x => (ushort)(p[i, x] < 0.6f ? 88 : 1)).ToArray();
                ModbusMessage msg = new WMRRequest(data, i*10);
                ModbusMessage ans = client.QA(msg);
                Console.WriteLine(ans.Function == msg.Function ? "ok" : "error");
            }

            client.Close();
        }
        
        static void FlagaMBSerial()
        {
            ModbusSerialClient client = new ModbusSerialClient("localhost");

            float[,] p = JasnośćObrazu("POLA0001.GIF", 10, 10);
            for (int i = 0; i < 10; i++)
            {
                ushort[] data = Enumerable.Range(0, 10).Select(x => (ushort)(p[i, x] < 0.6f ? 88 : 1)).ToArray();
                ModbusMessage msg = new WMRRequest(data, i*10);
                ModbusMessage ans = client.QA(msg);
                Console.WriteLine(ans.Function == msg.Function ? "ok" : "error");
            }

            client.Close();
        }

    }
}
