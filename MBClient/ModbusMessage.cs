﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBClient
{
    public class ModbusMessage
    {
        protected byte[] data;
        public byte[] Data { get { return data; } }

        public byte Function
        {
            get { return data[0]; }
            set { data[0] = value; }
        }

        public int Length
        {
            get { return data.Length; }
            set { data = new byte[value]; }
        }
    }
}
