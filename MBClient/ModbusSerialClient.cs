﻿using System.IO.Ports;
using System.Net.Sockets;

namespace MBClient
{
    class ModbusSerialClient : ModbusClient
    {
        string port;
        SerialPort client;
        NetworkStream stream;

        public ModbusSerialClient(string port)
        {
            this.port = port;

            client = new SerialPort(port);
//            stream = client.GetStream();
        }

        public override void Close()
        {
            stream.Close();
            client.Close();
        }

        static ushort transactionId = 0;
        public override void WriteRequest(ModbusMessage request)
        {
            byte[] header = new byte[7];
            header.SetTwoBytes(transactionId++, 0);
            header.SetTwoBytes(0, 2);
            header.SetTwoBytes((ushort)(1+request.Length), 4);
            header[6] = 0;
            stream.Write(header, 0, header.Length);
            stream.Write(request.Data, 0, request.Length);
        }

        public override ModbusMessage ReadMessage()
        {
            byte[] header = new byte[7];
            stream.Read(header, 0, header.Length);
            ushort length = header.GetTwoBytes(4);
            ModbusMessage msg = new ModbusMessage();
            msg.Length = length - 1;
            stream.Read(msg.Data, 0, msg.Length);
            return msg;
        }
    }
}
