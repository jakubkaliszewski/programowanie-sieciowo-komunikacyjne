﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBClient
{
    abstract class ModbusClient
    {
        public virtual ModbusMessage QA(ModbusMessage request)
        {
            WriteRequest(request);
            return ReadMessage();
        }

        public abstract void Close();
        public abstract void WriteRequest(ModbusMessage request);
        public abstract ModbusMessage ReadMessage();
    }
}
