#Zadania realizowane w ramach laboratorium:

1. System klient-serwer do różnego rodzaju komunikacji:
    ###Usługi
        - test szybkości transmisji i odpowiedzi (podobnie do ping)
        - wymiana wiadomości pomiędzy klientami
        - pobieranie plików z serwera, składowanie plików na serwerze
        - konfiguracja serwera (uruchamianie i wstrzymywanie usług, mediów komunikacyjnych, odpytywanie o dostępne usługi i media) 
    ###Media komunikacyjne:
        - pliki dyskowe
        - TCP
        - UDP
        - RS-232
        - .Net remoting
    ###Inne uwagi:
        - Serwer połączeń (np. TCP) - wielowątkowość - każde połączenie w osobnym wątku
        - Nasłuchiwanie połączeń a konkrente połączenia to dwie różne sprawy (jeden "nasłuchiwacz", możliwe wiele połączeń)
        - Kontrola poprawności transmisji
2. Tsunami UDP - transfer plików przez UDP i komunikacja kontrolna przez TCP
        - wysyłanie plików w paczkach mieszczących się w datagramie UDP
        - klient zleca pobranie pliku przez TCP i otrzymuje informacje o rozmiarze pliku i paczek
        - klient ponawia zapytanie o fragmenty, które nie dotarły
        - osobny wątek klienta do pobierania fragmentów (by zrównoleglić kontrolę przez TCP i pobieranie danych przez UDP)
        - serwer wielowątkowy dla obsługi wielu klientów równolegle
3. System do komunikacji protokołem MODBUS
        - najpierw z serwerem mod_RSsim
        - potem również własna emulacja serwera
        - wystarczy odczyt i zapis zmiennych binarnych i 16-bitowych
        - komunikacja przez MODBUS TCP
        - komunikacja RTU przez RS-232

