﻿using System;
using System.Threading;
using TsunamiShared.Session;

namespace Tsunami
{
    class Program
    {
        public static string SessionId;
        public static void Main()
        {
            while (true)
            {
                try
                {
                    //Połączenie inicjujące uzyskanie sesji i połączenie się z serwerem
              //      CreateInitialConnection();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                while (SessionId == null)
                {
                    Thread.Sleep(200);
                }
                ConsoleInterpreter.Start(SessionManager.Instance.GetSessionByID(SessionId));
            }
        }
/*
        private static void CreateInitialConnection()
        {
            InitialSessionFactory factory = new InitialSessionFactory();
            factory.CreateSession(TCP);
        }
*/
    }
}
