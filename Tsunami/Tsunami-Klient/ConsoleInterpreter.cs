using System;
using System.ComponentModel;
using System.Linq;
using Tsunami.Services;
using TsunamiShared;
using TsunamiShared.Services;
using TsunamiShared.Session;

namespace Tsunami
{
    public static class ConsoleInterpreter
    {
        private static bool _doCommand = true;
        private static Session _session;
        private static string _login;

        public static void Start(Session session)
        {
            if (session == null && session.Connection == null)
                throw new Exception("Sesja nie jest nawiązana!");
            _session = session;

            Console.WriteLine("Podaj swój login: ");
            _login = Console.ReadLine().Trim();

            while (_doCommand)
            {
                Console.Write($"{_login}$> ");
                var command = Console.ReadLine().Trim().ToLower();
                AnalyzeCommand(command);
            }
        }

        private static void AnalyzeCommand(string command)
        {
            var splittedCommand = command.Split(' ');
            var service = splittedCommand[0];
            var parameters = splittedCommand.Skip(1).ToArray();
            if (service.Equals("exit"))
            {
                ExitCommand();
                return;
            }

            if (service.Equals("poweroff"))
            {
                PowerOffCommand();
            }

            if (service.Equals(Description(ServicesEnum.CONFIG)))
            {
                ConfigService.Instance.DoAction(new Message(ServicesEnum.CONFIG.ToString("F"), _session.ID), parameters);
                return;
            }

            if (service.Equals(Description(ServicesEnum.FTP_SERVICE)))
            {
                FTPService.Instance.DoAction(new Message(ServicesEnum.FTP_SERVICE.ToString("F"), _session.ID), parameters);
                return;
            }
        }

        private static void PowerOffCommand()
        {
            ExitCommand();
            System.Environment.Exit(0);
        }

        private static void ExitCommand()
        {
            _doCommand = false; //TODO wysłanie wiadomości z EXIT
            _session.Close();
            _session = null;
        }

        private static string GetCustomDescription(object objEnum)
        {
            var field = objEnum.GetType().GetField(objEnum.ToString());
            var attributes = (DescriptionAttribute[]) field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description.ToLower() : objEnum.ToString().ToLower();
        }

        private static string Description(Enum value)
        {
            return GetCustomDescription(value);
        }
    }
}