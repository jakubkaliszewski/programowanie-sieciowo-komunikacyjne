﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Tsunami
{
    class Serwer
    {
        int portTCP;
        int portUDP;
        internal void Parametry(int portTCP, int portUDP)
        {
            this.portTCP = portTCP;
            this.portUDP = portUDP;
        }

        internal void Start()
        {
            UdpClient udpServer = new UdpClient(portUDP);

            TcpListener server = new TcpListener(IPAddress.Any, portTCP);
            server.Start();
            while (true)
            {
                TcpClient client = server.AcceptTcpClient(); // wielowątkowo!
                NetworkStream stream = client.GetStream();
                string polecenie = ReadLine(stream);
                Zadanie zadanie = new Zadanie(polecenie);
                WriteLine(stream, zadanie.Odpowiedź1());
                zadanie.Wysyłanie(udpServer);
                WriteLine(stream, zadanie.Odpowiedź2());
                client.Close();
            }
        }

        private void WriteLine(NetworkStream stream, string line)
        {
            byte[] msg = Encoding.ASCII.GetBytes(line+'\n');
            stream.Write(msg, 0, msg.Length);
        }

        byte[] bytes = new byte[256];
        string data = null;
        private string ReadLine(NetworkStream stream)
        {
            int len, nl;
            while ((len = stream.Read(bytes, 0, bytes.Length)) > 0)
            {
                data += Encoding.ASCII.GetString(bytes, 0, len);
                while ((nl = data.IndexOf('\n')) != -1)
                {
                    string line = data.Substring(0, nl + 1);
                    data = data.Substring(nl + 1);
                    return line;
                }
            }
            return null;
        }
    }
}
