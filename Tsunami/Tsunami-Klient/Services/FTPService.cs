using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TsunamiShared;
using TsunamiShared.Services;
using TsunamiShared.Services.FTP;
using TsunamiShared.Session;

namespace Tsunami.Services
{
    public sealed class FTPService : IService 
    {
        private static readonly FTPService _FTPServiceInstance = new FTPService();
        private bool _active = true;
        private List<ActiveTransfer> _transfers = new List<ActiveTransfer>();
        static FTPService(){}
        public static FTPService Instance
        {
            get { return _FTPServiceInstance; }
        }

        public void Start()
        {
            _active = true;
        }

        public void Stop()
        {
            _active = true;
        }

        public bool IsActive()
        {
            return _active;
        }

        /// <summary>
        /// Metoda wysyłająca wiadomość FTP_SERVICE do serwera.
        /// </summary>
        /// <param name="message">Wiadomość do przygotowania w ramach serwisu FTP_SERVICE.</param>
        /// <param name="parameters">Parametry wprowadzone z lini poleceń.</param>
        /// <exception cref="Exception">Wyjątek rzucany, gdy argumenty są nieprawidłowe.</exception>
        public void DoAction(Message message, string[] parameters)
        {
            string command = parameters[0];
            if (command.Equals("show"))
            {
                var messageToSend = PrepareShowMessage(message);
                SendResponse(messageToSend.GetSessionID(), messageToSend);
            }
            else
            {
                if (command.Equals("send") && parameters.Length == 2)
                    new Task(()=>SendFile(parameters[1], parameters[2], message.GetSessionID(), message.GetServiceName())).Start();
                else if (!command.Equals("get") && parameters.Length == 1)
                {
                    StringBuilder builder = new StringBuilder();
                    builder.Append("get").Append("|");
                    builder.Append(parameters[1]).Append("|");
                    
                    message.SetContent(builder.ToString());
                    message.SetChecksum();
                    SendResponse(message.GetSessionID(), message);
                }
                else
                    throw new Exception("Wprowadzono nieznaną akcję serwisu FTP!");
            }
        }

        /// <summary>
        /// Metoda odbierająca wiadomość dla serwisu. Zawiera dane do zapisu po stronie klienta jeśli zawiera flagę get.
        /// </summary>
        /// <param name="message">Odebrana wiadomość.</param>
        public void DoAction(Message message)
        {
            try
            {
                var content = message.GetContent();
                var split = content.Split('|');
                string flag = split[0];

                if (flag.Equals("show"))
                    ShowShowMessage(content);
                if (flag.Equals("get"))
                    SaveContent(message.GetSessionID(), content);
                if (flag.Equals("confirm"))
                    ConfirmPackage(content);
                else if (flag.Equals("end"))
                    EndTransfer(content);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Błąd przetwarzania wiadomości serwisu FTP!: {e.Message}");
            }
        }

        /// <summary>
        /// Metoda odbierająca wiadomość z flagą "end".
        /// </summary>
        /// <param name="content"></param>
        /// <exception cref="Exception"></exception>
        private void EndTransfer(string content)
        {
            try
            {
                var split = content.Split('|');
                string transferId = split[1];
                string lastPackageId = split[2];

                var actualTransfer = _transfers.Find(transfer => transfer.Id.ToString().Equals(transferId));
                if (actualTransfer == null)
                    throw new Exception($"Nie znaleziono aktywnego transferu o id {transferId}");

                actualTransfer.CloseTransfer(Int32.Parse(lastPackageId));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        /// <summary>
        /// Metoda wyświetlająca dostępne na serwerze zasoby.
        /// </summary>
        /// <param name="messageContent">Treść otrzymanej wiadomości.</param>
        private void ShowShowMessage(string messageContent)
        {
            Console.WriteLine("\n Dostępne zasoby na serwerze do pobrania:");
            var splitted = messageContent.Split('"');
            for (int i = 0; i < splitted.Length - 1; i++)
            {
                try
                {
                    var split = splitted[i].Split('|');
                    if (split.Length != 2)
                    {
                        throw new Exception("Brak pary nazwa zasobu | nazwa pliku!");
                    }

                    Console.WriteLine($"Nazwa: {split[0]}, plik: {split[1]}");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        /// <summary>
        /// Metoda zapisująca otrzymane dane pliku.
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="messageContent">Treść otrzymanej wiadomości.</param>
        private void SaveContent(string sessionId, string messageContent)
        {
            try
            {
                var split = messageContent.Split('|');
                Guid transferId = Guid.Parse(split[1]);
                int packageId = Int32.Parse(split[2]);
                string fileName = split[3];
                string bytes = split[4];

                var actualFilePackage = new FilePackage(packageId, bytes);
                ReceiveTransfer actualTransfer = (ReceiveTransfer) _transfers.Find(transfer => transfer.Id.Equals(transferId));
                actualTransfer.SetFileStream(fileName);

                actualTransfer.AddNewPackageToStream(actualFilePackage);
                SendPackageConfirmation(sessionId ,split[1], split[2]);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Błąd przy analizowaniu paczki danych: {e.Message}");
            }
        }

        /// <summary>
        /// Wysyła do serwera wiadomość otrzymania wiadomości z paczką.
        /// </summary>
        /// <param name="sessionId">Sesja</param>
        /// <param name="transferId">Identyfikator transmisji FTP</param>
        /// <param name="package">Identyfikator paczki, który ma zostać potwierdzony.</param>
        private void SendPackageConfirmation(string sessionId, string transferId, string package)//akcja confirm
        {
            Message confirmMessage = new Message(ServicesEnum.FTP_SERVICE.ToString("F"), sessionId);
            
            StringBuilder builder = new StringBuilder();
            builder.Append("confirm").Append("|");
            builder.Append(transferId).Append("|");
            builder.Append(package).Append("|");
            
            confirmMessage.SetContent(builder.ToString());
            confirmMessage.SetChecksum();
            SendResponse(sessionId, confirmMessage);
        }
        
        private void ConfirmPackage(string content)
        {
            try
            {
                var split = content.Split('|');
                string transferId = split[1];
                string packageId = split[2];

                var actualTransfer = _transfers.Find(transfer => transfer.Id.ToString().Equals(transferId));
                if (actualTransfer == null)
                    throw new Exception($"Nie znaleziono aktywnego transferu o id {transferId}");
                
                actualTransfer.ConfirmPackage(packageId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Wysyła do serwera żądanie pokazania dostępnych zasobów do pobrania na serwerze.
        /// </summary>
        /// <param name="message">Wiadomość do przygotowania.</param>
        /// <returns>Przygotowana wiadomość.</returns>
        private Message PrepareShowMessage(Message message)
        {
            message.SetContent("show");
            message.SetChecksum();
            return message;
        }

        private void SendFile(string resourceName, string pathToFile, string sessionID, string serviceName)
        {
            try
            {
                if (pathToFile == null || !IsFileExist(pathToFile))
                    throw new Exception($"Plik o ścieżce {pathToFile} niezbyt istnieje.");

                //już tu otwieram strumień
                var transfer = new SendTransfer(GetFileNameFromPath(pathToFile), sessionID, resourceName);
                _transfers.Add(transfer);
                int lastPackageId = 0;
                transfer.OnSendAgainUnconfirmedPackages += OnSendAgainUnconfirmedPackages;
                //odczytanie ze strumienia, tworzenie pojedynczej paczki
                //wysłanie
                //zapisanie paczki jako niepotwierdzonej w odbiorze
                foreach (FilePackage package in transfer.GetNextFilePackage())
                {
                    var message = PrepareSendMessage(sessionID, serviceName, resourceName, pathToFile, package, transfer.Id.ToString());
                    lastPackageId = package.PackageId;
                    new Task(() => SendResponse(message.GetSessionID(), message)).Start();
                }

                var endMessage = PrepareEndTransferMessage(sessionID, serviceName, transfer.Id.ToString(), lastPackageId);
                SendResponse(sessionID, endMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Wystąpił problem przy wysyłaniu pliku!\n{e.Message}");
            }
        }
        private void OnSendAgainUnconfirmedPackages(SendTransfer transfer)
        {
            foreach (FilePackage package in transfer.UnconfirmedPackages)
            {
                var message = PrepareSendMessage(transfer.SessionId, ServicesEnum.FTP_SERVICE.ToString("F"), transfer.ResourceName, "", package, transfer.Id.ToString());
                new Task(() => SendResponse(message.GetSessionID(), message)).Start();
            }

            Console.WriteLine("Wysłano ponownie niepotwierdzone pakiety!");
        }

        private Message PrepareEndTransferMessage(string sessionId, string serviceName, string transferId, int lastPackageId)
        {
            Message message = new Message(serviceName, sessionId);
            
            StringBuilder builder = new StringBuilder();
            builder.Append("end").Append("|");
            builder.Append(transferId).Append("|");
            builder.Append(lastPackageId).Append("|");
            
            message.SetContent(builder.ToString());
            message.SetChecksum();
            return message;
        }

        private Message PrepareSendMessage(string sessionId, string serviceName, string resourceName, string path, FilePackage package, string transferId)
        {
            var message = new Message(serviceName, sessionId);
            
            StringBuilder builder = new StringBuilder();
            builder.Append("send").Append("|");
            builder.Append(transferId).Append("|");
            builder.Append(package.PackageId).Append("|");
            builder.Append(GetFileNameFromPath(path)).Append("|");
            builder.Append(resourceName).Append("|");
            builder.Append(package.Data).Append("|");
            
            message.SetContent(builder.ToString());
            message.SetChecksum();
            return message;
        }

        private string GetFileNameFromPath(string path)
        {
            return Path.GetFileName(path) + "." + Path.GetExtension(path);
        }
        
        private bool IsFileExist(string path)
        {
            return File.Exists(path);
        }

        /// <summary>
        /// Otrzymanie instancji sesji na podstawie jej ID.
        /// </summary>
        /// <param name="sessionID">ID sesji.</param>
        /// <returns>Instancja sesji, która posiada podane ID.</returns>
        private Session GetSessionByID(string sessionID)
        {
            var sessionManager = SessionManager.Instance;
            return sessionManager.GetSessionByID(sessionID);
        }

        /// <summary>
        /// Zlecenie wysłania wiadomości - odpowiedzi w ramach podanej sesji.
        /// </summary>
        /// <param name="sessionID">Identyfikator sesji</param>
        /// <param name="messageToResponse">Wysyłana wiadomość</param>
        private void SendResponse(string sessionID, Message messageToResponse)
        {
            var sessionManager = SessionManager.Instance;
            sessionManager.SendMessage(sessionID, messageToResponse);
        }
    }
}