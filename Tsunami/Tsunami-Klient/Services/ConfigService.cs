using System;
using System.ComponentModel;
using System.Text;
using TsunamiShared;
using TsunamiShared.Medium;
using TsunamiShared.Services;
using TsunamiShared.Session;

namespace Tsunami.Services
{
    /// <summary>
    /// Serwis CONFIG po stronie klienta.
    /// </summary>
    public sealed class ConfigService : IService
    {
        private static readonly ConfigService _configServiceInstance = new ConfigService();
        private bool _active = true;

        static ConfigService()
        {
        }

        public static ConfigService Instance
        {
            get { return _configServiceInstance; }
        }

        /// <summary>
        /// Metoda odbierająca wiadomość dla serwisu, wyświetla status aktywnych usług oraz mediów.
        /// </summary>
        /// <param name="message">Odebrana wiadomość</param>
        public void DoAction(Message message)
        {
            Console.WriteLine("\nKonfiguracja serwera:");
            var messageContent = message.GetContent();
            var splitted = messageContent.Split('"');
            for (int i = 0; i < splitted.Length - 1; i++)
            {
                try
                {
                    var split = splitted[i].Split('|');
                    if (split.Length != 2)
                    {
                        throw new Exception("Brak pary medium/serwis | stan");
                    }

                    string status = split[1].Equals("True") ? "Aktywny" : "Dezaktywowany";
                    WriteOutputLine(split[0], status);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        private void WriteOutputLine(string mediumOrService, string status)
        {
            Console.Write($"{mediumOrService}: ");
            if (status.Equals("Aktywny"))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(status);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine(status);
            }

            Console.ResetColor();
        }

        public void Start()
        {
            _active = true;
        }

        public void Stop()
        {
            _active = false;
        }

        public bool IsActive()
        {
            return _active;
        }

        /// <summary>
        /// Metoda wysyłająca wiadomość CONFIG do serwera.
        /// </summary>
        /// <param name="message">Wiadomość do przygotowania w ramach serwisu CONFIG.</param>
        /// <param name="parameters">Parametry wprowadzone z lini poleceń.</param>
        /// <exception cref="Exception">Wyjątek rzucany, gdy argumenty są nieprawidłowe.</exception>
        public void DoAction(Message message, string[] parameters)
        {
            string command = parameters[0];
            if (command.Equals("show"))
            {
                var messageToSend = PrepareShowMessage(message);
                SendResponse(messageToSend.GetSessionID(), messageToSend);
            }
            else
            {
                if (!command.Equals("start") && !command.Equals("stop"))
                    throw new Exception("Wprowadzono nieznaną akcję konfiguracji!");
                if (parameters.Length != 2)
                    throw new Exception("Wprowadzono za mało argumentów by wykonać akcję konfiguracji!");
                var messageToSend = PrepareManagementMessage(message, command, parameters[1]);
                SendResponse(messageToSend.GetSessionID(), messageToSend);
            }
        }

        /// <summary>
        /// Wysyła do serwera żądanie pokazania statusu serwisów oraz mediów.
        /// </summary>
        /// <param name="message">Wiadomość do przygotowania.</param>
        /// <returns>Przygotowana wiadomość.</returns>
        private Message PrepareShowMessage(Message message)
        {
            message.SetContent("show");
            message.SetChecksum();
            return message;
        }

        /// <summary>
        /// Przygotowuje wiadomość sterującą zasobem na serwerze.
        /// </summary>
        /// <param name="message">Wiadomość do przygotowania.</param>
        /// <param name="active">Flag aktywacji/dezaktywacji</param>
        /// <param name="mediumOrService">Podane medium bądź serwis do sterowania.</param>
        /// <returns>Gotowa wiadomość do wysłania.</returns>
        /// <exception cref="Exception"></exception>
        private Message PrepareManagementMessage(Message message, string active, string mediumOrService)
        {
            if (!IsMediumExist(mediumOrService) && !IsServiceExist(mediumOrService))
                throw new Exception("Nie istnieje usługa ani medium o podanej nazwie!!");

            bool activeFlag;
            activeFlag = active.Equals("start");

            StringBuilder builder = new StringBuilder();
            builder.Append(mediumOrService).Append("|");
            builder.Append(activeFlag);

            message.SetContent(builder.ToString());
            message.SetChecksum();
            return message;
        }

        /// <summary>
        /// Metoda sprawdzająca czy medium o podanej nazwie istnieje.
        /// </summary>
        /// <param name="mediumName">Nazwa medium.</param>
        /// <returns>Rezultat</returns>
        private bool IsMediumExist(string mediumName)
        {
            mediumName = mediumName.ToLower();
            var enumValues = Enum.GetValues(typeof(MediumType));
            foreach (MediumType medium in (MediumType[]) enumValues)
            {
                if (mediumName.ToUpper().Equals(medium.ToString("F")))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Metoda sprawdzająca czy serwis o podanej nazwie istnieje.
        /// </summary>
        /// <param name="serviceName">Nazwa serwisu.</param>
        /// <returns>Rezultat</returns>
        private bool IsServiceExist(string serviceName)
        {
            serviceName = serviceName.ToLower();
            var enumValues = Enum.GetValues(typeof(ServicesEnum));
            foreach (ServicesEnum service in (ServicesEnum[]) enumValues)
            {
                if (serviceName.Equals(Description(service)))
                    return true;
            }

            return false;
        }

        private static string GetCustomDescription(object objEnum)
        {
            var field = objEnum.GetType().GetField(objEnum.ToString());
            var attributes = (DescriptionAttribute[]) field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description.ToLower() : objEnum.ToString().ToLower();
        }

        private static string Description(Enum value)
        {
            return GetCustomDescription(value);
        }

        /// <summary>
        /// Otrzymanie instancji sesji na podstawie jej ID.
        /// </summary>
        /// <param name="sessionID">ID sesji.</param>
        /// <returns>Instancja sesji, która posiada podane ID.</returns>
        private Session GetSessionByID(string sessionID)
        {
            var sessionManager = SessionManager.Instance;
            return sessionManager.GetSessionByID(sessionID);
        }

        /// <summary>
        /// Zlecenie wysłania wiadomości - odpowiedzi w ramach podanej sesji.
        /// </summary>
        /// <param name="sessionID">Identyfikator sesji</param>
        /// <param name="messageToResponse">Wysyłana wiadomość</param>
        private void SendResponse(string sessionID, Message messageToResponse)
        {
            var sessionManager = SessionManager.Instance;
            sessionManager.SendMessage(sessionID, messageToResponse);
        }
    }
}